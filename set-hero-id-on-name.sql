-- ALTER TABLE cards Add hero_id int not null;

SET SQL_SAFE_UPDATES = 0;
UPDATE cards
SET
	hero_id = 2
WHERE
	hero="Conquest, The First Rider of the End"