[Trace]   DarkRiftServer        System Details:
                                	OS: Microsoft Windows NT 6.2.9200.0
                                	CLS Version: 4.0.30319.42000
                                	DarkRift: 2.9.0.0 - Pro
[Trace]   PluginFactory         Created plugin 'HeroSyncGameManager'.
[Trace]   PluginManager         Plugin 'HeroSyncGameManager' has requested that DarkRift operates in thread safe mode.
[Trace]   DarkRiftServer        Switched into thread safe mode. Expect lower performance!
[Info]    PluginManager         Loaded plugin HeroSyncGameManager version 0.0.7
[Trace]   PluginFactory         Created plugin 'BichannelListener'.
[Trace]   HttpHealthCheck       HTTP health check started at 'http://localhost:10666/health'
[Trace]   BadWordFilter         Downloading bad word list from 'https://darkriftnetworking.com/DarkRift2/Resources/BadWords.xml'.
[Trace]   DarkRiftServer        Binding listeners to ClientManager as server is externally visible.
[Trace]   RemoteServerManager   No server registry connector configured, skipping registration.
[Trace]   DefaultNetworkListener Starting bichannel listener.
[Info]    DefaultNetworkListener Server mounted, listening on port 4296.
[Trace]   BadWordFilter         Loading bad word list.
[Trace]   BadWordFilter         Bad word list loaded sucessfully!
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:62677.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:56738.
[Info]    ClientManager         New client [0] connected [127.0.0.1:62677|127.0.0.1:56738].
[Info]    HeroSyncGameManager   About to start single player room
[Info]    HeroSyncGameManager   Starting single...
[Info]    HeroSyncGameManager   Found 17 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Found 18 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Received message from client. Has tag SUMMON_MONSTER
[Info]    HeroSyncGameManager   Received message from client. Has tag DEBUG_OPP_SUMMON_MONSTER
[Info]    HeroSyncGameManager   Received message from client. Has tag SET_COUNTER
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Info]    ClientManager         Client [0] disconnected.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:62690.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:62693.
[Info]    ClientManager         New client [1] connected [127.0.0.1:62690|127.0.0.1:62693].
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:62696.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:61188.
[Info]    ClientManager         New client [2] connected [127.0.0.1:62696|127.0.0.1:61188].
[Info]    HeroSyncGameManager   Received a message from the client with the JOIN_ROOM tag
[Info]    HeroSyncGameManager   Received a message from the client with the JOIN_ROOM tag
[Info]    HeroSyncGameManager   Received a message from the client with the SET_READY_TO_START tag
[Info]    HeroSyncGameManager   CHANGING ROOM BECAUSE SOMEONE CLICKED READY
[Info]    HeroSyncGameManager   Received a message from the client with the SET_READY_TO_START tag
[Info]    HeroSyncGameManager   CHANGING ROOM BECAUSE SOMEONE CLICKED READY
[Info]    HeroSyncGameManager   Starting Game Room
[Info]    HeroSyncGameManager   Found 17 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Found 18 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Received a message from the client with the GAME_ROOM_ENTERED tag
[Info]    HeroSyncGameManager   Receiving setup info
[Info]    HeroSyncGameManager   Received a message from the client with the GAME_ROOM_ENTERED tag
[Info]    HeroSyncGameManager   Receiving setup info
[Info]    HeroSyncGameManager   Sending info to other rooms
[Info]    HeroSyncGameManager   About to start single player room
[Info]    HeroSyncGameManager   Starting single...
[Info]    HeroSyncGameManager   Found 17 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Found 18 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Received a message from the client with the START_SINGLE_PLAYER_DEBUG tag
[Info]    HeroSyncGameManager   Received message from client. Has tag START_SINGLE_PLAYER_DEBUG
[Info]    HeroSyncGameManager   Received a message from the client with the DEBUG_FORCE_OPP_END_TURN tag
[Info]    HeroSyncGameManager   Received message from client. Has tag DEBUG_FORCE_OPP_END_TURN
[Info]    HeroSyncGameManager   Received message from client. Has tag DEBUG_FORCE_OPP_END_TURN
[Info]    ClientManager         Client [2] disconnected.
[Info]    ClientManager         Client [1] disconnected.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:62701.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:52967.
[Info]    ClientManager         New client [3] connected [127.0.0.1:62701|127.0.0.1:52967].
[Info]    HeroSyncGameManager   About to start single player room
[Info]    HeroSyncGameManager   Starting single...
[Info]    HeroSyncGameManager   Found 17 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Found 18 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Warning] ObjectCacheMonitor    4 AutoRecyclingArray objects were finalized last period. This is usually a sign that you are not recycling objects correctly.
[Warning] ObjectCacheMonitor    1 Message objects were finalized last period. This is usually a sign that you are not recycling objects correctly.
[Warning] ObjectCacheMonitor    14 MessageBuffer objects were finalized last period. This is usually a sign that you are not recycling objects correctly.
[Info]    ClientManager         Client [3] disconnected.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:61381.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:51625.
[Info]    ClientManager         New client [4] connected [127.0.0.1:61381|127.0.0.1:51625].
[Info]    HeroSyncGameManager   About to start single player room
[Info]    HeroSyncGameManager   Starting single...
[Info]    HeroSyncGameManager   Found 17 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Found 18 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    ClientManager         Client [4] disconnected.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:61386.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:50314.
[Info]    ClientManager         New client [5] connected [127.0.0.1:61386|127.0.0.1:50314].
[Info]    HeroSyncGameManager   Received a message from the client with the CREATE_ROOM tag
[Info]    HeroSyncGameManager   Received a message from the client with the JOIN_ROOM tag
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:61389.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:50341.
[Info]    ClientManager         New client [6] connected [127.0.0.1:61389|127.0.0.1:50341].
[Info]    HeroSyncGameManager   Received a message from the client with the JOIN_ROOM tag
[Info]    HeroSyncGameManager   Received a message from the client with the SET_READY_TO_START tag
[Info]    HeroSyncGameManager   CHANGING ROOM BECAUSE SOMEONE CLICKED READY
[Info]    HeroSyncGameManager   Received a message from the client with the SET_READY_TO_START tag
[Info]    HeroSyncGameManager   CHANGING ROOM BECAUSE SOMEONE CLICKED READY
[Info]    HeroSyncGameManager   Starting Game Room
[Info]    HeroSyncGameManager   Found 17 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Found 18 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Received a message from the client with the GAME_ROOM_ENTERED tag
[Info]    HeroSyncGameManager   Receiving setup info
[Info]    HeroSyncGameManager   About to start single player room
[Info]    HeroSyncGameManager   Starting single...
[Info]    HeroSyncGameManager   Found 17 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Found 18 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Received a message from the client with the START_SINGLE_PLAYER_DEBUG tag
[Info]    HeroSyncGameManager   Receiving setup info
[Info]    HeroSyncGameManager   Received a message from the client with the GAME_ROOM_ENTERED tag
[Info]    HeroSyncGameManager   Receiving setup info
[Info]    HeroSyncGameManager   Sending info to other rooms
[Info]    HeroSyncGameManager   Received a message from the client with the SUMMON_MONSTER tag
[Info]    HeroSyncGameManager   Received message from client. Has tag SUMMON_MONSTER
[Info]    HeroSyncGameManager   Received message from client. Has tag SUMMON_MONSTER
[Info]    HeroSyncGameManager   Received a message from the client with the END_TURN tag
[Info]    HeroSyncGameManager   Received message from client. Has tag END_TURN
[Info]    HeroSyncGameManager   Received message from client. Has tag END_TURN
[Info]    HeroSyncGameManager   Received a message from the client with the SUMMON_MONSTER tag
[Info]    HeroSyncGameManager   Received message from client. Has tag SUMMON_MONSTER
[Info]    HeroSyncGameManager   Received a message from the client with the SUMMON_MONSTER tag
[Info]    HeroSyncGameManager   Received message from client. Has tag SUMMON_MONSTER
[Info]    HeroSyncGameManager   Received a message from the client with the SET_COUNTER tag
[Info]    HeroSyncGameManager   Received message from client. Has tag SET_COUNTER
[Info]    HeroSyncGameManager   Received a message from the client with the END_TURN tag
[Info]    HeroSyncGameManager   Received message from client. Has tag END_TURN
[Info]    HeroSyncGameManager   Received a message from the client with the SUMMON_MONSTER tag
[Info]    HeroSyncGameManager   Received message from client. Has tag SUMMON_MONSTER
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the END_TURN tag
[Info]    HeroSyncGameManager   Received message from client. Has tag END_TURN
[Info]    HeroSyncGameManager   Received a message from the client with the SUMMON_MONSTER tag
[Info]    HeroSyncGameManager   Received message from client. Has tag SUMMON_MONSTER
[Info]    HeroSyncGameManager   Received a message from the client with the SUMMON_MONSTER tag
[Info]    HeroSyncGameManager   Received message from client. Has tag SUMMON_MONSTER
[Info]    HeroSyncGameManager   Received a message from the client with the SUMMON_MONSTER tag
[Info]    HeroSyncGameManager   Received message from client. Has tag SUMMON_MONSTER
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK_TARGET_REQUEST tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received a message from the client with the ATTACK tag
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Info]    ClientManager         Client [6] disconnected.
[Info]    ClientManager         Client [5] disconnected.
