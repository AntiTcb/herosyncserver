[Trace]   DarkRiftServer        System Details:
                                	OS: Microsoft Windows NT 6.2.9200.0
                                	CLS Version: 4.0.30319.42000
                                	DarkRift: 2.6.0.0 - Free
[Trace]   pluginFactory         Created plugin 'HeroSyncGameManager'.
[Trace]   PluginManager         Plugin 'HeroSyncGameManager' has requested that DarkRift operates in thread safe mode.
[Trace]   DarkRiftServer        Switched into thread safe mode. Expect lower performance!
[Info]    PluginManager         Loaded plugin HeroSyncGameManager version 0.0.4
[Trace]   pluginFactory         Created plugin 'BichannelListener'.
[Trace]   DefaultNetworkListener Starting bichannel listener.
[Info]    DefaultNetworkListener Server mounted, listening on port 4296.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:61573.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:56852.
[Info]    ClientManager         New client [0] connected [127.0.0.1:61573|127.0.0.1:56852].
[Info]    HeroSyncGameManager   Writing a log
[Info]    HeroSyncGameManager   Found 15 cards in the database
[Info]    ClientManager         Client [0] disconnected.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:61580.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:64748.
[Info]    ClientManager         New client [1] connected [127.0.0.1:61580|127.0.0.1:64748].
[Info]    HeroSyncGameManager   Writing a log
[Error]   Client                An plugin encountered an error whilst handling the MessageReceived event.
                                 System.InvalidOperationException: The connection is already open.
                                    at MySql.Data.MySqlClient.Interceptors.ExceptionInterceptor.Throw(Exception exception)
                                    at MySql.Data.MySqlClient.MySqlConnection.Open()
                                    at DBConnect.OpenConnection()
                                    at DBConnect.SelectCards(String query)
                                    at DBConnect.SelectCardsFromDeck(Int16 id)
                                    at HeroSyncServer.GameRoom.InitializeDeck(PlayerData player, Int16 deck_id)
                                    at HeroSyncServer.GameRoom.Start()
                                    at HeroSyncServer.GameRoom.CheckForSinglePlayer(Object sender, MessageReceivedEventArgs e)
                                    at DarkRift.Server.Client.<>c__DisplayClass45_0.<HandleIncomingMessage>b__0()
[Info]    ClientManager         Client [1] disconnected.
