[Trace]   DarkRiftServer        System Details:
                                	OS: Microsoft Windows NT 6.2.9200.0
                                	CLS Version: 4.0.30319.42000
                                	DarkRift: 2.6.0.0 - Free
[Trace]   pluginFactory         Created plugin 'HeroSyncGameManager'.
[Trace]   PluginManager         Plugin 'HeroSyncGameManager' has requested that DarkRift operates in thread safe mode.
[Trace]   DarkRiftServer        Switched into thread safe mode. Expect lower performance!
[Info]    PluginManager         Loaded plugin HeroSyncGameManager version 0.0.4
[Trace]   pluginFactory         Created plugin 'BichannelListener'.
[Trace]   DefaultNetworkListener Starting bichannel listener.
[Info]    DefaultNetworkListener Server mounted, listening on port 4296.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:52719.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:50630.
[Info]    ClientManager         New client [0] connected [127.0.0.1:52719|127.0.0.1:50630].
[Info]    HeroSyncGameManager   Writing a log
[Info]    HeroSyncGameManager   Found 15 cards in the database
[Info]    ClientManager         Client [0] disconnected.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:52722.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:53884.
[Info]    ClientManager         New client [1] connected [127.0.0.1:52722|127.0.0.1:53884].
[Info]    HeroSyncGameManager   Writing a log
[Info]    HeroSyncGameManager   Found 15 cards in the database
[Info]    ClientManager         Client [1] disconnected.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:52723.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:53885.
[Info]    ClientManager         New client [2] connected [127.0.0.1:52723|127.0.0.1:53885].
[Info]    HeroSyncGameManager   Writing a log
[Info]    HeroSyncGameManager   Found 15 cards in the database
[Info]    ClientManager         Client [2] disconnected.
