[Trace]   DarkRiftServer        System Details:
                                	OS: Microsoft Windows NT 6.2.9200.0
                                	CLS Version: 4.0.30319.42000
                                	DarkRift: 2.6.0.0 - Free
[Trace]   pluginFactory         Created plugin 'HeroSyncGameManager'.
[Trace]   PluginManager         Plugin 'HeroSyncGameManager' has requested that DarkRift operates in thread safe mode.
[Trace]   DarkRiftServer        Switched into thread safe mode. Expect lower performance!
[Info]    PluginManager         Loaded plugin HeroSyncGameManager version 0.0.3
[Trace]   pluginFactory         Created plugin 'BichannelListener'.
[Trace]   DefaultNetworkListener Starting bichannel listener.
[Info]    DefaultNetworkListener Server mounted, listening on port 4296.
[Trace]   CommandEngine         Command entered: 'sniffer add -a'
[Info]    Sniffer               Now sniffing all
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:53651.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:61608.
[Info]    ClientManager         New client [0] connected [127.0.0.1:53651|127.0.0.1:61608].
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:53652.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:61614.
[Info]    ClientManager         New client [1] connected [127.0.0.1:53652|127.0.0.1:61614].
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:53656.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:61534.
[Info]    ClientManager         New client [2] connected [127.0.0.1:53656|127.0.0.1:61534].
