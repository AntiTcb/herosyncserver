[Trace]   DarkRiftServer        System Details:
                                	OS: Microsoft Windows NT 6.2.9200.0
                                	CLS Version: 4.0.30319.42000
                                	DarkRift: 2.6.0.0 - Free
[Trace]   pluginFactory         Created plugin 'HeroSyncGameManager'.
[Trace]   PluginManager         Plugin 'HeroSyncGameManager' has requested that DarkRift operates in thread safe mode.
[Trace]   DarkRiftServer        Switched into thread safe mode. Expect lower performance!
[Info]    PluginManager         Loaded plugin HeroSyncGameManager version 0.0.7
[Trace]   pluginFactory         Created plugin 'BichannelListener'.
[Trace]   DefaultNetworkListener Starting bichannel listener.
[Info]    DefaultNetworkListener Server mounted, listening on port 4296.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:62731.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:53391.
[Info]    ClientManager         New client [0] connected [127.0.0.1:62731|127.0.0.1:53391].
[Info]    HeroSyncGameManager   Player attempting to log in.
[Info]    HeroSyncGameManager   Received a message from the client with the JOIN_ROOM tag
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:62741.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:65534.
[Info]    ClientManager         New client [1] connected [127.0.0.1:62741|127.0.0.1:65534].
[Info]    HeroSyncGameManager   Player attempting to log in.
[Info]    HeroSyncGameManager   Received a message from the client with the JOIN_ROOM tag
[Info]    HeroSyncGameManager   Received a message from the client with the SET_READY_TO_START tag
[Info]    HeroSyncGameManager   CHANGING ROOM BECAUSE SOMEONE CLICKED READY
[Info]    HeroSyncGameManager   Received a message from the client with the SET_READY_TO_START tag
[Info]    HeroSyncGameManager   CHANGING ROOM BECAUSE SOMEONE CLICKED READY
[Info]    HeroSyncGameManager   Found 17 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Found 18 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Received a message from the client with the START_SINGLE_PLAYER_DEBUG tag
[Info]    HeroSyncGameManager   Received a message from the client with the START_SINGLE_PLAYER_DEBUG tag
[Info]    HeroSyncGameManager   Starting single...
[Info]    HeroSyncGameManager   Found 17 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Found 18 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Error]   Client                An plugin encountered an error whilst handling the MessageReceived event.
                                 System.ArgumentNullException: Value cannot be null.
                                 Parameter name: s
                                    at System.Text.UnicodeEncoding.GetByteCount(String s)
                                    at DarkRift.DarkRiftWriter.Write(String value, Encoding encoding)
                                    at DarkRift.DarkRiftWriter.Write(String value)
                                    at HeroSyncCommon.PlayerData.Serialize(SerializeEvent e)
                                    at DarkRift.DarkRiftWriter.Write[T](T serializable)
                                    at DarkRift.Message.Create[T](UInt16 tag, T obj)
                                    at HeroSyncServer.GameRoom.StartSingle()
                                    at HeroSyncServer.GameRoom.CheckForSinglePlayer(Object sender, MessageReceivedEventArgs e)
                                    at System.EventHandler`1.Invoke(Object sender, TEventArgs e)
                                    at DarkRift.Server.Client.<>c__DisplayClass45_0.<HandleIncomingMessage>b__0()
[Info]    ClientManager         Client [0] disconnected.
[Info]    ClientManager         Client [1] disconnected.
