[Trace]   DarkRiftServer        System Details:
                                	OS: Microsoft Windows NT 6.2.9200.0
                                	CLS Version: 4.0.30319.42000
                                	DarkRift: 2.6.0.0 - Free
[Trace]   pluginFactory         Created plugin 'HeroSyncGameManager'.
[Trace]   PluginManager         Plugin 'HeroSyncGameManager' has requested that DarkRift operates in thread safe mode.
[Trace]   DarkRiftServer        Switched into thread safe mode. Expect lower performance!
[Info]    PluginManager         Loaded plugin HeroSyncGameManager version 0.0.7
[Trace]   pluginFactory         Created plugin 'BichannelListener'.
[Trace]   DefaultNetworkListener Starting bichannel listener.
[Info]    DefaultNetworkListener Server mounted, listening on port 4296.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:55308.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:57653.
[Info]    ClientManager         New client [0] connected [127.0.0.1:55308|127.0.0.1:57653].
[Info]    HeroSyncGameManager   About to start single player room
[Info]    HeroSyncGameManager   Starting single...
[Info]    HeroSyncGameManager   Found 17 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Found 18 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Received message from client. Has tag SUMMON_MONSTER
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    ClientManager         Client [0] disconnected.
