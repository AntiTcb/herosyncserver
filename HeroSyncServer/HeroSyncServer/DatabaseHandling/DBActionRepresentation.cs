﻿using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;

namespace HeroSyncServer.DatabaseHandling
{
    public class DBActionRepresentation
    {
        public ActionData.ActionTypeEnum cardActionEnum;
        public int ID;
        public TargetAlgorithm targetAlgo;
        public bool targetHasCondition;
        public List<DBConditionCategoryRepresentation> targetConditions;
        //TODO: Add target condition based on location;

        public enum TargetAlgorithm
        {
            Self = 1,
            OwningPlayer = 2,
            OpposingPlayer = 3,
            WeakestFollower = 4,
            StrongestFollower = 5,
            PlayersChoice = 6,
            OpponentsChoice = 7,
            NoTarget=999
        }
    }
}
