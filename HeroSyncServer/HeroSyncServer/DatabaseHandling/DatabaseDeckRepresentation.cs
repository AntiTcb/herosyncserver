﻿using HeroSyncCommon;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace HeroSyncServer.DatabaseHandling
{
    public class DatabaseDeckRepresentation
    {
        public short deck_id;
        public short user_id;
        public string username;
        public string deckname;
        public short main_1;
        public short main_2;
        public short main_3;
        public short main_4;
        public short main_5;
        public short main_6;
        public short main_7;
        public short main_8;
        public short main_9;
        public short main_10;
        public short main_11;
        public short main_12;
        public short main_13;
        public short main_14;
        public short main_15;
        public short main_16;
        public short main_17;
        public short main_18;
        public short main_19;
        public short main_20;
        public short main_21;
        public short main_22;
        public short main_23;
        public short main_24;
        public short main_25;
        public short main_26;
        public short main_27;
        public short main_28;
        public short main_29;
        public short main_30;
        public short main_31;
        public short main_32;
        public short main_33;
        public short main_34;
        public short main_35;
        public short main_36;
        public short main_37;
        public short main_38;
        public short main_39;
        public short main_40;
        public short main_41;
        public short main_42;
        public short main_43;
        public short main_44;
        public short main_45;
        public short hero_id;

        internal void ConstructDeck(List<CardData> allCards, Deck playerDeck, bool isBlue)
        {
            List<CardData> tempDeckList = new List<CardData>();
            tempDeckList.Add(allCards.Find(x => x.ID == main_1));
            tempDeckList.Add(allCards.Find(x => x.ID == main_2));
            tempDeckList.Add(allCards.Find(x => x.ID == main_3));
            tempDeckList.Add(allCards.Find(x => x.ID == main_4));
            tempDeckList.Add(allCards.Find(x => x.ID == main_5));
            tempDeckList.Add(allCards.Find(x => x.ID == main_6));
            tempDeckList.Add(allCards.Find(x => x.ID == main_7));
            tempDeckList.Add(allCards.Find(x => x.ID == main_8));
            tempDeckList.Add(allCards.Find(x => x.ID == main_9));
            tempDeckList.Add(allCards.Find(x => x.ID == main_10));
            tempDeckList.Add(allCards.Find(x => x.ID == main_11));
            tempDeckList.Add(allCards.Find(x => x.ID == main_12));
            tempDeckList.Add(allCards.Find(x => x.ID == main_13));
            tempDeckList.Add(allCards.Find(x => x.ID == main_14));
            tempDeckList.Add(allCards.Find(x => x.ID == main_15));
            tempDeckList.Add(allCards.Find(x => x.ID == main_16));
            tempDeckList.Add(allCards.Find(x => x.ID == main_17));
            tempDeckList.Add(allCards.Find(x => x.ID == main_18));
            tempDeckList.Add(allCards.Find(x => x.ID == main_19));
            tempDeckList.Add(allCards.Find(x => x.ID == main_20));
            tempDeckList.Add(allCards.Find(x => x.ID == main_21));
            tempDeckList.Add(allCards.Find(x => x.ID == main_22));
            tempDeckList.Add(allCards.Find(x => x.ID == main_23));
            tempDeckList.Add(allCards.Find(x => x.ID == main_24));
            tempDeckList.Add(allCards.Find(x => x.ID == main_25));
            tempDeckList.Add(allCards.Find(x => x.ID == main_26));
            tempDeckList.Add(allCards.Find(x => x.ID == main_27));
            tempDeckList.Add(allCards.Find(x => x.ID == main_28));
            tempDeckList.Add(allCards.Find(x => x.ID == main_29));
            tempDeckList.Add(allCards.Find(x => x.ID == main_30));
            tempDeckList.Add(allCards.Find(x => x.ID == main_31));
            tempDeckList.Add(allCards.Find(x => x.ID == main_32));
            tempDeckList.Add(allCards.Find(x => x.ID == main_33));
            tempDeckList.Add(allCards.Find(x => x.ID == main_34));
            tempDeckList.Add(allCards.Find(x => x.ID == main_35));
            tempDeckList.Add(allCards.Find(x => x.ID == main_36));
            tempDeckList.Add(allCards.Find(x => x.ID == main_37));
            tempDeckList.Add(allCards.Find(x => x.ID == main_38));
            tempDeckList.Add(allCards.Find(x => x.ID == main_39));
            tempDeckList.Add(allCards.Find(x => x.ID == main_40));
            tempDeckList.Add(allCards.Find(x => x.ID == main_41));
            tempDeckList.Add(allCards.Find(x => x.ID == main_42));
            tempDeckList.Add(allCards.Find(x => x.ID == main_43));
            tempDeckList.Add(allCards.Find(x => x.ID == main_44));
            tempDeckList.Add(allCards.Find(x => x.ID == main_45));

            List<CardData> deckList = new List<CardData>();
            for (int i = 0; i < tempDeckList.Count; i++)
            {
                CardData baseCard = tempDeckList[i];
                CardData copiedCard = baseCard.CreateCardCopy((short) (i + (!isBlue ? 0 : 100)));
                deckList.Add(copiedCard);
                if (copiedCard == null)
                {
                    Console.Error.Write("CARD COPYING FAILED, ADDED A NULL TO A DECK OF CARDS. CRASH IMMINENT\n");
                }
            }

            playerDeck.cards = deckList;
        }
    }

    public static class CardCopier
    {
        public static CardData CreateCardCopy(this CardData data, short gameID)
        {
            CardData copy = new CardData();
            copy.strength = data.strength;
            copy.level = data.level;
            copy.ID = data.ID;
            copy.type = data.type;
            copy.heroID = data.heroID;
            copy.englishName = data.englishName;
            copy.englishDescription = data.englishDescription;
            copy.gameID = gameID;
            return copy;
        }
    }
}