﻿using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;

namespace HeroSyncServer
{
    // TODO: This class needs to be updated to use the actual db of players.

    static class PlayerDatabase
    {
        public static PlayerData GetNewPlayerData(Account account)
        {
            if (account.playerData != null)
            {
                return account.playerData;
            }

            else
            {
                PlayerData playerData = new PlayerData();
                playerData.playerId = account.accountID;
                playerData.cardsInHand = new List<CardData>();
                playerData.username = account.accountName;
                return playerData;
            }
        }
    }
}
