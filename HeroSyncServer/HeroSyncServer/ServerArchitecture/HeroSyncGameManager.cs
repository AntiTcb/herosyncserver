﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using DarkRift;
using DarkRift.Server;
using HeroSyncCommon;
using HeroSyncCommon.GameManagement;

namespace HeroSyncServer
{
    public class HeroSyncGameManager : Plugin
    {
        public Dictionary<Account, GameRoom> activeRooms;
        public Dictionary<Account, IClient> clientPlayerDataDictionary;
        public Dictionary<Account, RoomInfo> waitingRooms;
        public List<IClient> clientsInLobby;
        public List<Account> accountsInLobby;
        public RoomSet rooms;
        public DBConnect herosyncDbConnect;

        public override bool ThreadSafe => false;

        public override Version Version => new Version(0, 0, 7);

        public HeroSyncGameManager(PluginLoadData pluginLoadData) : base(pluginLoadData)
        {
            herosyncDbConnect = new DBConnect(this.Logger);
            clientPlayerDataDictionary = new Dictionary<Account, IClient>();
            activeRooms = new Dictionary<Account, GameRoom>();
            rooms = new RoomSet();
            waitingRooms = new Dictionary<Account, RoomInfo>();
            accountsInLobby = new List<Account>();
            RoomInfo defaultEmptyRoom = new RoomInfo();
            defaultEmptyRoom.currentState = RoomInfo.RoomState.EMPTY;
            defaultEmptyRoom.name = "Default Room";
            rooms.rooms = new List<RoomInfo>();
            rooms.rooms.Add(defaultEmptyRoom);
            clientsInLobby = new List<IClient>();
            ClientManager.ClientConnected += ClientConnected;
            ClientManager.ClientDisconnected += ClientDisconnected;
        }

        void ClientConnected(object sender, ClientConnectedEventArgs e)
        {
            e.Client.MessageReceived += PlayerLoggedIn;
            e.Client.MessageReceived += StartSingePlayerDebug;
        }

        private void StartSingePlayerDebug(object sender, MessageReceivedEventArgs e)
        {
            if (e.Tag == (short) NetworkStateTags.ClientMessageTags.START_SINGLE_PLAYER_DEBUG)
            {
                GameRoom singlePlayerRoom = new GameRoom();
                singlePlayerRoom.dbConnect = herosyncDbConnect;
                Account a = new Account();
                a.accountID = -1;
                a.accountName = "Test";
                PlayerData singlePlayer = PlayerDatabase.GetNewPlayerData(a);
                singlePlayerRoom.AddBluePlayer(e.Client, singlePlayer);
                Account x = new Account();
                x.accountName = "No Action CPU";
                x.accountID = -2;
                PlayerData otherPlayer = PlayerDatabase.GetNewPlayerData(x);
                singlePlayerRoom.AddOrangePlayer(null, otherPlayer);
                Logger.Info("About to start single player room");
                singlePlayerRoom.StartSingle(this);
            }
        }

        void PlayerLoggedIn(object message, MessageReceivedEventArgs e)
        {
            if (e.Tag == (short) NetworkStateTags.ClientMessageTags.SEND_LOGIN_INFO)
            {
                IClient loggedInClient = e.Client;
                clientsInLobby.Add(loggedInClient);
                LoginInfo lInfo = e.GetMessage().Deserialize<LoginInfo>();
                Account tempAccount = new Account();
                tempAccount.accountID = lInfo.id;
                tempAccount.accountName = lInfo.username;
                tempAccount.playerData = PlayerDatabase.GetNewPlayerData(tempAccount);
                if (clientPlayerDataDictionary.ContainsValue(loggedInClient))
                {
                    Logger.Warning("Client tried to log in twice. This should not happen.");
                    return;
                }
                while (clientPlayerDataDictionary.Keys.Any(x => x.accountID == tempAccount.accountID))
                {
                    Logger.Warning("Tried to log in with an ID that's already present. Going to give a different ID");
                    Random rand = new Random();
                    short randId = (short) rand.Next(0, short.MaxValue);
                    tempAccount.accountID = randId;
                }
                
                clientPlayerDataDictionary.Add(tempAccount, loggedInClient);
                accountsInLobby.Add(tempAccount);
                SendMessage(lInfo, NetworkStateTags.ServerMessageTags.ACCEPT_LOGIN, loggedInClient);
                loggedInClient.MessageReceived += ClientRoomInputManagement;
                loggedInClient.MessageReceived -= PlayerLoggedIn;
                SendMessage(rooms, NetworkStateTags.ServerMessageTags.LIST_ROOMS, e.Client);
            }
        }

        void ClientRoomInputManagement(object message, MessageReceivedEventArgs e)
        {
            NetworkStateTags.ClientMessageTags tag = (NetworkStateTags.ClientMessageTags) e.Tag;
            string entryMessage = String.Format("Received a message from the client with the {0} tag", tag.ToString());
            Logger.Info(entryMessage);
            switch (tag)
            {
                case NetworkStateTags.ClientMessageTags.LOGOUT:
                    LoginInfo logOutInfo = e.GetMessage().Deserialize<LoginInfo>();
                    LogOut(logOutInfo, e.Client);
                    break;
                case NetworkStateTags.ClientMessageTags.CREATE_ROOM:
                    RoomInfo addedRoomInfo = e.GetMessage().Deserialize<RoomInfo>();
                    CreateRoom(addedRoomInfo);
                    break;
                case NetworkStateTags.ClientMessageTags.DELETE_ROOM:
                    RoomInfo removedRoomInfo = e.GetMessage().Deserialize<RoomInfo>();
                    RemoveRoom(removedRoomInfo);
                    break;
                case NetworkStateTags.ClientMessageTags.JOIN_ROOM:
                    JoinRoomRequest joinRoomRequest = e.GetMessage().Deserialize<JoinRoomRequest>();
                    HandleJoinRoomRequest(joinRoomRequest);
                    break;
                case NetworkStateTags.ClientMessageTags.SET_READY_TO_START:
                    RoomInfo changedRoom = e.GetMessage().Deserialize<RoomInfo>();
                    RoomInfo modifiedRoom = ModifyRoom(changedRoom);
                    CheckRoomForStart(modifiedRoom);
                    break;
                case NetworkStateTags.ClientMessageTags.LEAVE_ROOM:
                    Account leavingAccount = e.GetMessage().Deserialize<Account>();
                    if (waitingRooms.ContainsKey(leavingAccount))
                    {
                        RoomInfo roomToLeave = waitingRooms[leavingAccount];
                        LeaveRoom(leavingAccount, roomToLeave);
                    }
                    break;
            }
        }

        private void CheckRoomForStart(RoomInfo modifiedRoom)
        {
            bool hasBothPlayers = modifiedRoom.bluePlayer != null && modifiedRoom.orangePlayer != null &&
                                  modifiedRoom.orangePlayer != modifiedRoom.bluePlayer;

            bool bothPlayersAreReady = modifiedRoom.orangeReady && modifiedRoom.blueReady;
            if (hasBothPlayers && bothPlayersAreReady)
            {
                StartGame(modifiedRoom);
            }
        }

        private void StartGame(RoomInfo room)
        {
            PlayerData bluePlayer = room.bluePlayer.playerData;
            PlayerData orangePlayer = room.orangePlayer.playerData;

            bluePlayer.playerColor = PlayerData.PlayerColor.BLUE;
            orangePlayer.playerColor = PlayerData.PlayerColor.ORANGE;

            IClient blueClient = clientPlayerDataDictionary[room.bluePlayer];
            IClient orangeClient = clientPlayerDataDictionary[room.orangePlayer];

            SendMessage(bluePlayer, NetworkStateTags.ServerMessageTags.GAME_START, blueClient);
            SendMessage(orangePlayer, NetworkStateTags.ServerMessageTags.GAME_START, orangeClient);

            clientsInLobby.Remove(blueClient);
            clientsInLobby.Remove(orangeClient);

            accountsInLobby.Remove(room.bluePlayer);
            accountsInLobby.Remove(room.orangePlayer);

            waitingRooms.Remove(room.bluePlayer);
            waitingRooms.Remove(room.orangePlayer);

            RemoveRoom(room);

            GameRoom newGameRoom = new GameRoom();
            newGameRoom.AddBluePlayer(blueClient, bluePlayer);
            newGameRoom.AddOrangePlayer(orangeClient, orangePlayer);
            newGameRoom.dbConnect = herosyncDbConnect;
            newGameRoom.Start(this);

            // TODO: More figuring out goes here. Or maybe not?
        }

        private RoomInfo ModifyRoom(RoomInfo changedRoom)
        {
            Logger.Info("CHANGING ROOM BECAUSE SOMEONE CLICKED READY");
            RoomInfo localRoom = rooms.rooms.Find(x => x.ID == changedRoom.ID);
            if (localRoom == null)
            {
                Logger.Error("Tried to change a room that doesn't exist. Creating...");
                CreateRoom(changedRoom);
                return changedRoom;
            }

            localRoom.bluePlayer = changedRoom.bluePlayer;
            localRoom.orangePlayer = changedRoom.orangePlayer;
            localRoom.name = changedRoom.name;
            localRoom.blueReady = changedRoom.blueReady;
            localRoom.orangeReady = changedRoom.orangeReady;
            SendMessageAll(rooms, NetworkStateTags.ServerMessageTags.LIST_ROOMS);
            return localRoom;
        }

        private void CreateRoom(RoomInfo newRoom)
        {
            Random idGen = new Random();
            newRoom.ID = idGen.Next();
            while (rooms.rooms.Any(x => x.ID == newRoom.ID))
            {
                newRoom.ID = idGen.Next();
            }
            rooms.rooms.Add(newRoom);
            newRoom.currentState = RoomInfo.RoomState.EMPTY;
            SendMessageAll(rooms, NetworkStateTags.ServerMessageTags.LIST_ROOMS);
        }

        private void RemoveRoom(RoomInfo removedRoomInfo)
        {
            foreach (RoomInfo room in rooms.rooms)
            {
                if (room.ID == removedRoomInfo.ID)
                {
                    rooms.rooms.Remove(room);
                    break;
                }
            }
            SendMessageAll(rooms, NetworkStateTags.ServerMessageTags.LIST_ROOMS);
        }

        private void HandleJoinRoomRequest(JoinRoomRequest request)
        {
            RoomInfo roomToJoin = rooms.rooms.Find(x => x.ID == request.roomID);
            if (roomToJoin == null)
            {
                string missingRoomWarning = String.Format("Player tried to access a room with ID {0} that doesn't seem to exist.", request.roomID);
                Logger.Warning(missingRoomWarning);
                return; //TODO: No reason we couldn't auto-gen a room for them instead, but I'll do that if I have more time later.
            }

            Account accountToJoinRoom = accountsInLobby.Find(x => x.accountID == request.accountID);
            if (accountToJoinRoom == null)
            {
                string missingAccountWarning = String.Format("Recieved a join request for an account with a missing ID {0}??", request.accountID);
                Logger.Warning(missingAccountWarning);
                return;
            }

            if (waitingRooms.ContainsKey(accountToJoinRoom))
            {
                RoomInfo previousRoom = waitingRooms[accountToJoinRoom];
                LeaveRoom(accountToJoinRoom, previousRoom);
            }

            if (roomToJoin.bluePlayer == null)
            {
                roomToJoin.bluePlayer = accountToJoinRoom;
            } else if (roomToJoin.orangePlayer == null)
            {
                roomToJoin.orangePlayer = accountToJoinRoom;
            }
            else
            {
                string roomFullError = String.Format("Room {0} is already full and cannot be joined.", roomToJoin.name);
                Logger.Error(roomFullError);
                return;
            }

            waitingRooms.Add(accountToJoinRoom, roomToJoin);
            SendMessageAll(rooms, NetworkStateTags.ServerMessageTags.LIST_ROOMS);
        }

        private void LeaveRoom(Account account, RoomInfo room)
        {
            if (account.Equals(room.bluePlayer))
            {
                room.bluePlayer = null;
                waitingRooms.Remove(account);
            }
            else if (account.Equals(room.orangePlayer))
            {
                room.orangePlayer = null;
                waitingRooms.Remove(account);
            }
            else
            {
                string errMessage = String.Format("Tried to leave room {0} but wasn't in room", room.name);
                Logger.Error(errMessage);
            }
            SendMessageAll(rooms, NetworkStateTags.ServerMessageTags.LIST_ROOMS);
        }


        private void LogOut(LoginInfo info, IClient loggedOutClient)
        {
            Account loggedOutAccount = clientPlayerDataDictionary.Keys.ToList().Find(x => x.accountID == info.id);
            if (loggedOutAccount == null)
            {
                Logger.Warning("Games Manager was missing an Account-IClient Pair.");
                loggedOutAccount = clientPlayerDataDictionary.Keys.ToList()
                    .Find(x => loggedOutClient == clientPlayerDataDictionary[loggedOutAccount]);
                if (loggedOutAccount == null)
                {
                    Logger.Error("Account-IClient pair was never added to the dictionary. Something has gone very wrong in HeroSyncGameManager");
                }
            }

            accountsInLobby.Remove(loggedOutAccount);
            clientPlayerDataDictionary.Remove(loggedOutAccount);
            clientsInLobby.Remove(loggedOutClient);
            loggedOutClient.MessageReceived -= ClientRoomInputManagement;
            loggedOutClient.MessageReceived += PlayerLoggedIn;
            if (waitingRooms.ContainsKey(loggedOutAccount))
            {
                LeaveRoom(loggedOutAccount, waitingRooms[loggedOutAccount]);
            }
        }


        void ClientDisconnected(object sender, ClientDisconnectedEventArgs e)
        {
            // TODO: Do management things
        }

        void SendMessageAll(IDarkRiftSerializable serializedObject, NetworkStateTags.ServerMessageTags messageTag)
        {
            foreach (IClient client in clientsInLobby)
            {
                SendMessage(serializedObject, messageTag, client);
            }
        }

        void SendMessage(IDarkRiftSerializable serializedObject, NetworkStateTags.ServerMessageTags messageTag, IClient client)
        {
            using (Message message = Message.Create((ushort) (messageTag), serializedObject))
            {
                client.SendMessage(message, SendMode.Reliable);
            }
        }
    }
}
