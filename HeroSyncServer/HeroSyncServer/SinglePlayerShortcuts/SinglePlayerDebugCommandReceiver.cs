﻿using HeroSyncCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DarkRift;
using DarkRift.Server;
using HeroSyncServer.GameLogic.InputHandlers;

namespace HeroSyncServer.SinglePlayerShortcuts
{
    public class SinglePlayerDebugCommandReceiver
    {
        private Dictionary<NetworkStateTags.ClientMessageTags, List<InputHandlerInterface>> inputHandlerDictionary;

        private GameRoom gr;

        public void HandleDebugInput(object sender, MessageReceivedEventArgs e)
        {
            this.HandleMessage((NetworkStateTags.ClientMessageTags)e.Tag, e.GetMessage(), gr);
        }

        public SinglePlayerDebugCommandReceiver(GameRoom debugRoom)
        {
            gr = debugRoom;
            inputHandlerDictionary = new Dictionary<NetworkStateTags.ClientMessageTags, List<InputHandlerInterface>>();
            // This is where all of the instances of the handlers are manually added to the dictionary. 
            var type = typeof(InputHandlerInterface);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p));

            List<InputHandlerInterface> handlers = new List<InputHandlerInterface>();

            foreach (var handlerType in types)
            {
                // String message = String.Format("Checking to add {0} as a handler.\n", handlerType.Name);
                // Console.Write(message);
                if (!handlerType.IsInterface)
                {
                    // Console.Write("Success!\n\n");
                    InputHandlerInterface newHandler = (InputHandlerInterface)Activator.CreateInstance(handlerType);
                    if (newHandler.DebugOnly())
                    {
                        handlers.Add(newHandler);
                    }
                }
            }

            foreach (InputHandlerInterface handler in handlers)
            {
                List<NetworkStateTags.ClientMessageTags> handledTags = handler.GetHandledTags();
                foreach (NetworkStateTags.ClientMessageTags handledTag in handledTags)
                {
                    if (!inputHandlerDictionary.ContainsKey(handledTag))
                    {
                        inputHandlerDictionary.Add(handledTag, new List<InputHandlerInterface>());
                    }
                    inputHandlerDictionary[handledTag].Add(handler);
                }
            }
        }

        public void HandleMessage(NetworkStateTags.ClientMessageTags tag, Message m, GameRoom gr)
        {
            string msg = String.Format("Received message with tag {0}, checking for relevant debug handler.", tag.ToString());
            Console.WriteLine(msg);
            if (inputHandlerDictionary.ContainsKey(tag))
            {
                List<InputHandlerInterface> handlers = inputHandlerDictionary[tag];
                foreach (InputHandlerInterface handler in handlers)
                {
                    handler.HandleInput(m, gr);
                }
            }
        }
    }
}
