﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DarkRift;
using HeroSyncCommon;
using HeroSyncCommon.NetworkActions;
using HeroSyncServer.GameLogic.EventProcessing.Events;
using HeroSyncServer.GameLogic.InputHandlers;

namespace HeroSyncServer.SinglePlayerShortcuts
{
    public class OpponentSummonWeakMonster : InputHandlerInterface
    {
        public bool DebugOnly()
        {
            return true;
        }

        public List<NetworkStateTags.ClientMessageTags> GetHandledTags()
        {
            return new List<NetworkStateTags.ClientMessageTags>() {NetworkStateTags.ClientMessageTags.DEBUG_OPP_SUMMON_MONSTER};
        }

        public void HandleInput(Message m, GameRoom gr)
        {
            OptionSet o = m.Deserialize<OptionSet>();
            int cardId = 0;
            if (o.cardOptions.Count > 0)
            {
                cardId = o.cardOptions[0].gameID;
            }

            CardData card = gr.orangePlayer.playerDeck.cards.Find(x => x.gameID == cardId);
            if (card == null)
            {
                card = gr.orangePlayer.playerDeck.cards.First(x => x.type == CardType.Follower && x.level == 1);
            }

            SummonEvent se = new SummonEvent(gr.orangePlayer, card, PlayerField.Zone.OrangeFollowerSlot1);
            gr.AddEventToStack(se);
        }
    }
}
