﻿using DarkRift;
using HeroSyncCommon;
using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon.NetworkActions;
using HeroSyncServer.GameLogic.EventProcessing.Events;

namespace HeroSyncServer.GameLogic.InputHandlers
{
    public class DebugEndOpponentTurnInputHandler : InputHandlerInterface
    {
        public bool DebugOnly()
        {
            return true;
        }

        public List<NetworkStateTags.ClientMessageTags> GetHandledTags()
        {
            return new List<NetworkStateTags.ClientMessageTags>() {NetworkStateTags.ClientMessageTags.DEBUG_FORCE_OPP_END_TURN };
        }

        public void HandleInput(Message m, GameRoom gr)
        {
            EmptyMessage playerMessage = m.Deserialize<EmptyMessage>();
            PlayerData player = playerMessage.sender;
            player = player.playerId == gr.orangePlayer.playerId ? gr.orangePlayer : gr.bluePlayer;
            EndTurnEvent ete = new EndTurnEvent(player);
            gr.AddEventToStack(ete);
        }
    }
}
