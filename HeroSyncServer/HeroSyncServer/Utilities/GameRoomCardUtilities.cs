﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.GameLogic;
using HeroSyncServer.GameLogic;

namespace HeroSyncServer.Utilities
{
    public static class GameRoomCardUtilities
    {
        public static void AddCardToHand(this GameRoom gr, CardData card, PlayerData player)
        {
            gr.cardStatusDictionary[card].location = CardLocation.Hand;
            CardPermissionCalculator.SetCardPermissions(card, gr.cardStatusDictionary[card]);
            PlayerField.Zone handZone = PlayerField.Zone.BlueHand;
            if (player.playerColor != PlayerData.PlayerColor.BLUE)
            {
                handZone = PlayerField.Zone.OrangeHand;
            }
            gr.cardStatusDictionary[card].currentZone = handZone;
            player.cardsInHand.Add(card);
            player.playerDeck.cards.Remove(card);
        }
    }
}
