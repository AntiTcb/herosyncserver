﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using HeroSyncServer.GameLogic.GameActions;

namespace HeroSyncServer.GameLogic.EventProcessing.Processors
{
    public class EventStack
    {
        private List<Event> eventsToCheckAfter;
        private Stack<Event> currentWhenEvents;
        private Stack<Event> afterEffectsToProcess;
        private List<Event> history;
        private ActionFactory actionFactory;

        public EventProcessorList processorList;

        public EventStack(EventProcessorList startingList, GameRoom gr)
        {
            if (startingList == null)
            {
                throw new NoNullAllowedException("Passed a null instance of an EventProcessorList to EventStack.");
            }

            processorList = startingList;
            eventsToCheckAfter = new List<Event>();
            currentWhenEvents = new Stack<Event>();
            afterEffectsToProcess = new Stack<Event>();
            
            history = new List<Event>();
            actionFactory = new ActionFactory(gr);
        }

        public void AddToStack(Event e)
        {
            Console.WriteLine("Adding event {0} to event Stack, top of add to stack method.", e);
            currentWhenEvents.Push(e);
            history.Add(e);
            processorList.ProcessWhenEvent(e);

            if (!e.IsCancelled())
            {
                GameAction action = actionFactory.CreateAction(e.GetActionType());
                if (action == null)
                {
                    Console.Error.WriteLine("Missing action for action enum {0}", e.GetActionType().ToString());
                }

                action.TakeAction(e);
            }

            currentWhenEvents.Pop();
            Console.WriteLine("Finished running through the when events and finished the action related to event {0}", e);

            if (!e.IsCancelled())
            {
                eventsToCheckAfter.Add(e);
            }

            if (currentWhenEvents.Count <= 0)
            {
                Stack<Event> newAfterEvents = processorList.GetNewlyTriggeredAfterEvents(eventsToCheckAfter);
                eventsToCheckAfter.Clear();
                int eventsToCheckCount = newAfterEvents.Count;
                for (int i = 0; i < eventsToCheckCount; i++) {
                    afterEffectsToProcess.Push(newAfterEvents.Pop());
                }

                if (afterEffectsToProcess.Count > 0)
                {
                    Event nextAfterEvent = afterEffectsToProcess.Pop();
                    AddToStack(nextAfterEvent);
                }
            }
        }
    }
}
