﻿using System;
using System.Collections.Generic;
using HeroSyncCommon;
using HeroSyncServer.GameLogic.EventProcessing.Events;
using HeroSyncServer.GameLogic.GameActions;
using HeroSyncServer.GameLogic.Targeting;

namespace HeroSyncServer.GameLogic.EventProcessing.Processors
{
    public class EventProcessor
    {
        protected GameRoom game;

        public Object processorObject;

        public List<EventTriggerEvaluator> triggers;
        public List<(Event, Targeter)> actionEvents;
        public List<CardLocation> legalTriggerLocation;
        public Timing timing;
        public Guid ID;

        public bool IsRelevant(Event e)
        {
            if (processorObject != null && processorObject.GetType() == typeof(CardServerData))
            {
                CardServerData csd = (CardServerData) processorObject;
                if (!legalTriggerLocation.Contains(csd.location))
                {
                    return false;
                }
            }

            bool isRelevant = EvaluateSpecificEvent(e);
            if (!isRelevant && e.GetType() == typeof(CompositeEvent))
            {
                CompositeEvent composite = (CompositeEvent) e;
                int index = 0;
                while (!isRelevant && index < composite.GetEvents().Count)
                {
                    Event compEvent = composite.GetEvents()[index];
                    isRelevant = EvaluateSpecificEvent(compEvent);
                    index++;
                }
            }

            return isRelevant;
        }

        private bool EvaluateSpecificEvent(Event e)
        {
            bool isRelevant = true;
            for (int i = 0; i < triggers.Count && isRelevant; i++)
            {
                isRelevant = triggers[i].EvaluateEvent(e);
            }

            return isRelevant;
        }


        public EventProcessor(GameRoom g)
        {
            game = g;
            ID = Guid.NewGuid();
            triggers = new List<EventTriggerEvaluator>();
            actionEvents = new List<(Event, Targeter)>();
        }

    }
}
