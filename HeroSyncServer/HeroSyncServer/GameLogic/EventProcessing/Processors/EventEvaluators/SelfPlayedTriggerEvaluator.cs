﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace HeroSyncServer.GameLogic.EventProcessing.Processors.EventEvaluators
{
    public class SelfPlayedTriggerEvaluator : EventTriggerEvaluator
    {
        public override bool EvaluateEvent(Event e)
        {
            return false;
        }

        public override ActionData.ActionTypeEnum ReleventTriggerType()
        {
            return ActionData.ActionTypeEnum.NoAction;
        }

        public SelfPlayedTriggerEvaluator() : base(null, null) { }
    }
}
