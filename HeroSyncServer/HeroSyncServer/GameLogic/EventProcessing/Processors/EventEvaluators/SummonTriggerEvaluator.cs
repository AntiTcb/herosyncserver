﻿using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.DatabaseHandling;
using HeroSyncServer.GameLogic.EventProcessing.Events;

namespace HeroSyncServer.GameLogic.EventProcessing.Processors.EventEvaluators
{
    public class SummonTriggerEvaluator : EventTriggerEvaluator
    {
        public SummonTriggerEvaluator() : base(null, null)
        {
            // Do nothing, this is only called by the trigger factory.
        }

        public SummonTriggerEvaluator(DBConditionRepresentation dbData,
            EventTriggerEvaluator baseEvaluator) : base(dbData, baseEvaluator) { }

        public override bool EvaluateEvent(Event e)
        {
            if (e.GetType() == typeof(SummonEvent))
            {
                SummonEvent summonIntent = (SummonEvent) e;
                bool meetsConditions = true;
                CardData target = summonIntent.card;
                meetsConditions = meetsConditions && CheckTargetsValidity(target);
                meetsConditions = meetsConditions && CheckSourceValidity(e.GetSource());
                return meetsConditions && baseEvaluator.EvaluateEvent(e);
            }
            return false;
        }

        public override ActionData.ActionTypeEnum ReleventTriggerType()
        {
            return ActionData.ActionTypeEnum.SummonFollower;
        }
    }
}
