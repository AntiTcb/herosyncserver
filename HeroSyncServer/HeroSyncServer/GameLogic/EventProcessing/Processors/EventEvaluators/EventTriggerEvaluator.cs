﻿using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.DatabaseHandling;
using HeroSyncServer.GameLogic.EventProcessing.Processors.EventEvaluators;

namespace HeroSyncServer.GameLogic.EventProcessing.Processors
{
    /**
     * This abstract class represents an object that evaluates whether or not an object should trigger on an event.
     * It's implementations need to be able to evaluate an event, and if relevant set the source & target conditions.
     * but this class provides the infrastructure for evaluating those things once the child class determines what they are.
     */

    public abstract class EventTriggerEvaluator
    {
        protected EventTriggerEvaluator baseEvaluator;
        protected bool hasTargetCondition = false;
        protected bool hasSourceCondition = false;
        protected List<ObjectCondition> targetConditions;
        protected List<ObjectCondition> sourceConditions;

        public abstract bool EvaluateEvent(Event e);
        public abstract ActionData.ActionTypeEnum ReleventTriggerType();

        public EventTriggerEvaluator(DBConditionRepresentation dbData, EventTriggerEvaluator baseEvaluator)
        {
            if (dbData == null || baseEvaluator == null)
            {
                return;
            }

            this.baseEvaluator = baseEvaluator;
            if (dbData.specifyTargetCategories)
            {
                hasTargetCondition = true;
                targetConditions = new List<ObjectCondition>();
                foreach (DBConditionCategoryRepresentation dbRepresentation in dbData.targetCategories)
                {
                    ObjectCondition @object = new ObjectCondition();
                    @object.SetValuesFromDatabase(dbRepresentation);
                    targetConditions.Add(@object);
                }
            }
            if (dbData.specifySourceCategories)
            {
                hasSourceCondition = true;
                sourceConditions = new List<ObjectCondition>();
                foreach (DBConditionCategoryRepresentation dbRepresentation in dbData.sourceCategories)
                {
                    ObjectCondition @object = new ObjectCondition();
                    @object.SetValuesFromDatabase(dbRepresentation);
                    sourceConditions.Add(@object);
                }
            }
        }


        /**
         * This function runs through the list of target conditions if the evaluator has any.
         */
        protected bool CheckTargetsValidity(Object target)
        {
            bool meetsConditions = true;
            if (hasTargetCondition)
            {
                foreach (ObjectCondition condition in targetConditions)
                {
                    meetsConditions = meetsConditions && condition.MeetsCondition(target);
                }
            }
            return meetsConditions;
        }

        /**
         * This function runs through the list of source conditions if the evaluator has any.
         */
        protected bool CheckSourceValidity(Object source)
        {
            bool meetsConditions = true;
            if (hasSourceCondition)
            {
                foreach (ObjectCondition condition in sourceConditions)
                {
                    meetsConditions = meetsConditions && condition.MeetsCondition(source);
                }
            }
            return meetsConditions;
        }
    }
}
