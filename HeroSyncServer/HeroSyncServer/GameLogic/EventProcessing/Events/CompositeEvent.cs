﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeroSyncServer.GameLogic.EventProcessing.Events
{
    public class CompositeEvent : Event
    {
        private List<Event> underlyingEvents;

        public override ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.CompositeAction;
        }

        public CompositeEvent(Object src, List<Event> events) : base(src)
        {
            underlyingEvents = events;
        }

        public void AddEvent(Event e)
        {
            underlyingEvents.Add(e);
        }

        public List<Event> GetEvents()
        {
            return underlyingEvents;
        }

        public List<ActionData.ActionTypeEnum> GetUnderlyingTypes()
        {
            List<ActionData.ActionTypeEnum> types = underlyingEvents.ConvertAll<ActionData.ActionTypeEnum>(x => x.GetActionType());
            return types;
        }
    }
}
