﻿using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.DatabaseHandling;

namespace HeroSyncServer.GameLogic.EventProcessing.Events
{
    public class EndTurnEvent : Event
    {
        public bool newPlayerShouldDraw;

        public EndTurnEvent(Object source) : base(source)
        {
            newPlayerShouldDraw = true;
        }

        public override ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.EndTurn;
        }

    }
}
