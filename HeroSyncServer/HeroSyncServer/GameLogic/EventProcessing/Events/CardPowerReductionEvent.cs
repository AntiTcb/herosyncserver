﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.DatabaseHandling;

namespace HeroSyncServer.GameLogic.EventProcessing.Events
{
    public class CardPowerReductionEvent : Event
    {
        public int powerReduction;
        public CardData cardToReduce;

        public CardPowerReductionEvent(Object source, CardData target, int reductionAmount) : base(source)
        {
            powerReduction = reductionAmount;
            cardToReduce = target;
            this.target = target;
        }

        public override ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.WeakenFollower;
        }

    } 
}
