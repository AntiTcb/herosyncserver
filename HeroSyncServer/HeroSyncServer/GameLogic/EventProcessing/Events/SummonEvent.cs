﻿using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.DatabaseHandling;

namespace HeroSyncServer.GameLogic.EventProcessing.Events
{
    public class SummonEvent : Event
    {
        public CardData card;
        public PlayerField.Zone targetedZone;

        public SummonEvent(Object source, CardData card, PlayerField.Zone zone) : base(source)
        {
            this.card = card;
            this.targetedZone = zone;
            this.target = card;
        }


        public override ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.SummonFollower;
        }
    }
}
