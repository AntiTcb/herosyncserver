﻿using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;

namespace HeroSyncServer.GameLogic.EventProcessing.Events
{
    public class PlayDriveEvent : Event
    {
        public CardData playedCard;

        public PlayDriveEvent(Object source, CardData card) : base(source)
        {
            playedCard = card;
            target = card;
        }

        public override ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.PlayDrive;
        }
    }
}
