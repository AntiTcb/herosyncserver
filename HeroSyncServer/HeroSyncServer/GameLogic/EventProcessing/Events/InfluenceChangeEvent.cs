﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;

namespace HeroSyncServer.GameLogic.EventProcessing.Events
{
    public class InfluenceChangeEvent : Event
    {
        public short amount;
        public PlayerData playerTarget;

        public override ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.InfChange;
        }

        public InfluenceChangeEvent(Object source, PlayerData playerTarget, short amount) : base(source)
        {
            this.amount = amount;
            this.playerTarget = playerTarget;
        }
    }
}
