﻿using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;

namespace HeroSyncServer.GameLogic
{
    public static class AddCounterPermissions
    {
        public static void AddPermissions(CardData card, CardServerData status)
        {
            status.UpdateLevelLegality();
            if (status.location == CardLocation.Hand)
            {
                card.availableActions.Add(CardActionEnum.SetCounter);
            }

            // else if (status.location == CardLocation.Field && status.activationLegallity)
            // {
            //     // TODO: This is almost certainly wrong and only applies if the counter can activate at any time.
            //     card.availableActions.Add(CardAction.ActivateCounter);
            // }
        }
    }
}
