﻿using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;

namespace HeroSyncServer.GameLogic
{
    public static class AddFollowerPermissions
    {
        public static void AddPermissions(CardData card, CardServerData status)
        {
            status.UpdateLevelLegality();
            if (status.location == CardLocation.Hand)
            {
                if (status.activationLegallity) 
                { 
                    card.availableActions.Add(CardActionEnum.SummonFollower);
                }
            }

            if (status.location == CardLocation.Field && !status.properties.Contains(Property.HAS_ATTACKED))
            {
                card.availableActions.Add(CardActionEnum.Attack);
            }

        }
    }
}
