﻿using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;

namespace HeroSyncServer.GameLogic
{
    public static class AddDrivePermissions
    {
        public static void AddPermissions(CardData card, CardServerData status)
        {
            status.UpdateLevelLegality();
            if (status.location == CardLocation.Hand && status.activationLegallity)
            {
                card.availableActions.Add(CardActionEnum.Play);
            }
        }
    }
}
