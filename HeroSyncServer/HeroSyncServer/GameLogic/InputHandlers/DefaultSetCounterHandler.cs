﻿using DarkRift;
using HeroSyncCommon;
using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon.NetworkActions;

namespace HeroSyncServer.GameLogic.InputHandlers
{
    public class DefaultSetCounterHandler : InputHandlerInterface
    {
        private List<NetworkStateTags.ClientMessageTags> handledTags;

        public DefaultSetCounterHandler()
        {
            handledTags = new List<NetworkStateTags.ClientMessageTags>();
            handledTags.Add(NetworkStateTags.ClientMessageTags.SET_COUNTER);
        }

        public List<NetworkStateTags.ClientMessageTags> GetHandledTags()
        {
            return handledTags;
        }

        public void HandleInput(Message m, GameRoom gr)
        {
            SetCounter setCounterAction = m.Deserialize<SetCounter>();
            CardData card = setCounterAction.cardToSet;
            PlayerField.Zone targetedZone = setCounterAction.targetedZone;
            PlayerData.PlayerColor playerColor = setCounterAction.player;

            if (!CanSetCounter(setCounterAction)) return;

            if (gr.AddCardToZone(card, targetedZone))
            {
                MoveCard moveCardMessage = new MoveCard();
                moveCardMessage.cardToMove = card;
                moveCardMessage.location = targetedZone;
                moveCardMessage.showCardBeforeMoving = false;
                moveCardMessage.cardHidden = true;
                gr.SendMessage(moveCardMessage,  NetworkStateTags.ServerMessageTags.MOVE_CARD_TO_FIELD , SendMode.Reliable);
            }
        }

        public bool CanSetCounter(SetCounter action)
        {
            CardData card = action.cardToSet;
            if (!card.availableActions.Contains(CardActionEnum.SetCounter))
            {
                Console.Error.Write("Received a request from the client to set a card that doesn't currently have the set counter gameAction available.");
                return false;
            }
            return true;
        }

        public bool DebugOnly()
        {
            return false;
        }
    }
}
