﻿using DarkRift;
using HeroSyncCommon;
using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon.NetworkActions;
using HeroSyncServer.GameLogic.EventProcessing.Events;
using HeroSyncServer.GameLogic.Targeting;

namespace HeroSyncServer.GameLogic.InputHandlers
{
    public class AttackTargetHandler : InputHandlerInterface
    {
        public List<NetworkStateTags.ClientMessageTags> GetHandledTags()
        {
            return new List<NetworkStateTags.ClientMessageTags>() {NetworkStateTags.ClientMessageTags.ATTACK};
        }

        public void HandleInput(Message m, GameRoom gr)
        {
            FollowerAttackMessage fam = m.Deserialize<FollowerAttackMessage>();
            Console.WriteLine("Recieved an attack target request with {0} type", fam.targetType);
            if (fam.targetType == FollowerAttackMessage.TargetType.CARD)
            {
                AttackTargetEvent targetEvent = new AttackTargetEvent(fam.attackingPlayer, fam.attackingCard, fam.targetedCard);
                if (gr == null)
                {
                    Console.Error.WriteLine("The Attack Target handler has somehow been passed a null gameroom???");
                }
                gr.AddEventToStack(targetEvent);
                Console.WriteLine("Attack target event finished, going to call add event to stack for target event's resulting attack.");
                AttackEvent attackEvent = targetEvent.resultingEvent;
                gr.AddEventToStack(attackEvent);
            }
            else
            {
                AttackTargetEvent targetEvent = new AttackTargetEvent(fam.attackingPlayer, fam.attackingCard, fam.targetedHero);
                gr.AddEventToStack(targetEvent);
                AttackEvent attackEvent = targetEvent.resultingEvent;
                gr.AddEventToStack(attackEvent);

            }

        }
        public bool DebugOnly()
        {
            return false;
        }
    }
}
