﻿using DarkRift;
using HeroSyncCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using HeroSyncCommon.NetworkActions;

namespace HeroSyncServer.GameLogic.InputHandlers
{
    public class AttackRequestHandler : InputHandlerInterface
    {
        public List<NetworkStateTags.ClientMessageTags> GetHandledTags()
        {
             return new List<NetworkStateTags.ClientMessageTags>() {NetworkStateTags.ClientMessageTags.ATTACK_TARGET_REQUEST};
        }

        public void HandleInput(Message m, GameRoom gr)
        {
            OptionSet attackTargets = new OptionSet();
            AttackTargetRequest atr = m.Deserialize<AttackTargetRequest>();
            CardData attackingCard = atr.attackingCard;
            List<CardData> legalCardTargets = GetAllLegalAttackCardTargets(attackingCard, gr);
            List<CharacterData> legalCharacterTargets = GetAllLegalAttackHeroTargets(attackingCard, gr, legalCardTargets.Count > 0);
            attackTargets.cardOptions = legalCardTargets;
            attackTargets.characterOptions = legalCharacterTargets;
            attackTargets.canBeCancelled = true;
            attackTargets.ListBookkeeping();
            gr.SendMessage(attackTargets,  NetworkStateTags.ServerMessageTags.OPTION_SET, SendMode.Reliable);
        }

        private List<CharacterData> GetAllLegalAttackHeroTargets(CardData attackingCard, GameRoom gr, bool hasLegalCardTargets)
        {
            List<CharacterData> legalTarget = new List<CharacterData>();
            if (!hasLegalCardTargets)
            {
                CardServerData card = gr.cardStatusDictionary[attackingCard];
                PlayerData.PlayerColor attackingColor = card.owner.playerColor;
                CharacterData hero = gr.GetPlayer(attackingColor).playerHero;
                legalTarget.Add(hero);
            }
            return legalTarget;
        }

        // TODO: Implement checks for effects that prevent certain cards from being targetable.
        private bool EvaluateTargetLegality(CardData cardOnField, GameRoom gr)
        {
            CardServerData cardServerData = gr.GetCardServerData(cardOnField);
            return !cardServerData.properties.Contains(Property.HIDDEN);
        }

        private List<CardData> GetAllLegalAttackCardTargets(CardData attackingCard, GameRoom gr)
        {
            CardServerData card = gr.cardStatusDictionary[attackingCard];
            PlayerData.PlayerColor attackingColor = card.owner.playerColor;
            PlayerData.PlayerColor defendingColor = attackingColor == PlayerData.PlayerColor.BLUE
                ? PlayerData.PlayerColor.ORANGE
                : PlayerData.PlayerColor.BLUE;

            List<CardData> allCardsOnField = gr.cardStatusDictionary.Keys.Where(x =>
                gr.cardStatusDictionary[x].location == CardLocation.Field &&
                gr.cardStatusDictionary[x].owner.playerColor == defendingColor).ToList();

            List<CardData> targetable = new List<CardData>();
            foreach (CardData cardOnField in allCardsOnField)
            {
                bool isLegalTarget = EvaluateTargetLegality(cardOnField, gr);
                if (isLegalTarget)
                {
                    targetable.Add(cardOnField);
                }
            }

            return targetable;
        }

        public bool DebugOnly()
        {
            return false;
        }
    }
}
