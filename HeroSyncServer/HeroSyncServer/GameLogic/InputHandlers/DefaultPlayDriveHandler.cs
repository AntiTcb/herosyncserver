﻿using DarkRift;
using HeroSyncCommon;
using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon.GameLogic;
using HeroSyncCommon.NetworkActions;

namespace HeroSyncServer.GameLogic.InputHandlers
{
    class DefaultPlayDriveHandler : InputHandlerInterface
    {
        private List<NetworkStateTags.ClientMessageTags> handledTags;
        public List<NetworkStateTags.ClientMessageTags> GetHandledTags()
        {
            return handledTags;
        }

        public DefaultPlayDriveHandler()
        {
            handledTags = new List<NetworkStateTags.ClientMessageTags>();
            handledTags.Add(NetworkStateTags.ClientMessageTags.PLAY_DRIVE);
        }


        public void HandleInput(Message m, GameRoom gr)
        {
            PlayDrive playDriveAction = m.Deserialize<PlayDrive>();
            CardData card = playDriveAction.cardToPlay;
            if (card.level > gr.GetPlayer(playDriveAction.playerColor).playerHero.level)
            {
                Console.Error.Write("Tried to play a drive that the hero was too low level to play.");
                return;
            }

            // Need to activate card effect here

            PlayerField.Zone fallenArea = gr.GetFallenArea(playDriveAction.playerColor);
            if (gr.AddCardToZone(card, fallenArea))
            {
                MoveCard moveCardMessage = new MoveCard();
                moveCardMessage.cardToMove = card;
                moveCardMessage.location = fallenArea;
                moveCardMessage.showCardBeforeMoving = true;
                gr.SendMessage(moveCardMessage, NetworkStateTags.ServerMessageTags.MOVE_CARD_TO_FALLEN, SendMode.Reliable);
            }
        }

        public bool DebugOnly()
        {
            return false;
        }
    }
}
