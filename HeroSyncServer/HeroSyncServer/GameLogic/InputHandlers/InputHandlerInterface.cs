﻿using System;
using System.Collections.Generic;
using System.Text;
using DarkRift;
using HeroSyncCommon;
using Renci.SshNet;

namespace HeroSyncServer.GameLogic.InputHandlers
{
    public interface InputHandlerInterface
    {
        List<NetworkStateTags.ClientMessageTags> GetHandledTags();
        void HandleInput(Message m, GameRoom gr);
        bool DebugOnly();
    }
}
