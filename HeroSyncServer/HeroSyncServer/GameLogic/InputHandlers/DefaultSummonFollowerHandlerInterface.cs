﻿using DarkRift;
using HeroSyncCommon;
using HeroSyncCommon.NetworkActions;
using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon.GameLogic;
using HeroSyncServer.GameLogic.EventProcessing;
using HeroSyncServer.GameLogic.EventProcessing.Events;

namespace HeroSyncServer.GameLogic.InputHandlers
{
    public class DefaultSummonFollowerHandlerInterface : InputHandlerInterface
    {
        private List<NetworkStateTags.ClientMessageTags> handledTags;

        public List<NetworkStateTags.ClientMessageTags> GetHandledTags()
        {
            return handledTags;
        }

        public DefaultSummonFollowerHandlerInterface()
        {
            handledTags = new List<NetworkStateTags.ClientMessageTags>();
            handledTags.Add(NetworkStateTags.ClientMessageTags.SUMMON_MONSTER);
        }

        public void HandleInput(Message m, GameRoom gr)
        {
            Console.WriteLine("Using Default Summon Handle Input");
            SummonFollower summonFollowerAction = m.Deserialize<SummonFollower>();
            CardData card = summonFollowerAction.cardToSummon;
            PlayerField.Zone zone = summonFollowerAction.targetedZone;
            if (CanBeSummoned(summonFollowerAction, gr))
            {
                PlayerData player = gr.GetPlayer(summonFollowerAction.player);
                SummonEvent sie = new SummonEvent(player, card, zone);
                gr.AddEventToStack(sie);
            }
            else
            {
                Console.WriteLine("Card could not be summoned.");
            }
        }

        private bool CanBeSummoned(SummonFollower action, GameRoom gr)
        {
            CardData card = action.cardToSummon;

            if(!card.availableActions.Contains(CardActionEnum.SummonFollower))
            {
                Console.Error.WriteLine("Received a request from the client to summon a card that doesn't currently have the summon Follower gameAction available.");
                return false;
            }

            PlayerData.PlayerColor playerColor = action.player;
            PlayerData player = gr.GetPlayer(playerColor);

            if (card.level > player.playerHero.level)
            {
                Console.Error.WriteLine("Tried to play a card with a level higher than the current available level based on turn count.");
                return false;
            }

            return true;
        }

        public bool DebugOnly()
        {
            return false;
        }
    }
}
