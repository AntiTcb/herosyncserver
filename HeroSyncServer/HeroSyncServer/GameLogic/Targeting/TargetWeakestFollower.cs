﻿using HeroSyncServer.DatabaseHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HeroSyncCommon;
using HeroSyncServer.GameLogic.EventProcessing.Processors.EventEvaluators;

namespace HeroSyncServer.GameLogic.Targeting
{
    public class TargetWeakestFollower : Targeter
    {
        public override DBActionRepresentation.TargetAlgorithm GetTargetAlgorithm()
        {
            return DBActionRepresentation.TargetAlgorithm.WeakestFollower;
        }

        public override List<Object> GetTargets(GameRoom gr)
        {
            List<CardData> cards = gr.cardStatusDictionary.Keys.ToList();
            var cardsAsObjects = cards.Cast<Object>().ToList();
            List<Object> prunedObjects = PruneOnConditions(cardsAsObjects);
            List<CardData> validCardDatas = prunedObjects.Cast<CardData>().ToList();

            if (validCardDatas.Count == 0)
            {
                Console.WriteLine("Target Weakest Follower failed to find a target that met it's conditions.");
                return new List<object>();
            }

            validCardDatas = validCardDatas.Where(x => x.type == CardType.Follower || x.type == CardType.Token).ToList();

            validCardDatas.Sort((a, b) => (a.strength.CompareTo(b.strength)));
            short lowestStrength = validCardDatas[0].strength;
            validCardDatas = validCardDatas.Where(c => c.strength == lowestStrength).ToList();
            return validCardDatas.Cast<Object>().ToList();
        }
    }
}
