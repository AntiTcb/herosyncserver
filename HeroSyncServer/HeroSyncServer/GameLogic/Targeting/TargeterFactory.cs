﻿using HeroSyncServer.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncServer.DatabaseHandling;
using HeroSyncServer.GameLogic.EventProcessing.Processors.EventEvaluators;

namespace HeroSyncServer.GameLogic.Targeting
{
    public class TargeterFactory
    {
        private Dictionary<DBActionRepresentation.TargetAlgorithm, Type> targetAlgorithmDictionary;

        public Targeter GetTargeter(DBActionRepresentation actionRepresentation)
        {
            DBActionRepresentation.TargetAlgorithm actionTargetAlgo = actionRepresentation.targetAlgo;
            Targeter ti = (Targeter)Activator.CreateInstance(targetAlgorithmDictionary[actionTargetAlgo]);
            List<ObjectCondition> conditions = new List<ObjectCondition>();
            if (actionRepresentation.targetHasCondition)
            {
                foreach (DBConditionCategoryRepresentation dbCondition in actionRepresentation.targetConditions)
                {
                    ObjectCondition condition = new ObjectCondition();
                    condition.SetValuesFromDatabase(dbCondition);
                    conditions.Add(condition);
                }
            }

            return ti;
        }

        public TargeterFactory()
        {
            targetAlgorithmDictionary = new Dictionary<DBActionRepresentation.TargetAlgorithm, Type>();
            var types = GetImplementationUtility.GetImplementations<Targeter>();

            foreach (var targeterType in types)
            {
                if (!(targeterType.IsInterface || targeterType.IsAbstract))
                {
                    Targeter ti = (Targeter)Activator.CreateInstance(targeterType);
                    DBActionRepresentation.TargetAlgorithm algorithmBase = ti.GetTargetAlgorithm();
                    targetAlgorithmDictionary.Add(algorithmBase, targeterType);
                }
            }
        }
    }
}
