﻿using HeroSyncServer.DatabaseHandling;
using System;
using System.Collections.Generic;
using System.Text;

namespace HeroSyncServer.GameLogic.Targeting
{
    public class NoTargeter : Targeter
    {
        public override DBActionRepresentation.TargetAlgorithm GetTargetAlgorithm()
        {
            return DBActionRepresentation.TargetAlgorithm.NoTarget;
        }

        public override List<object> GetTargets(GameRoom gr)
        {
            return null;
        }
    }
}
