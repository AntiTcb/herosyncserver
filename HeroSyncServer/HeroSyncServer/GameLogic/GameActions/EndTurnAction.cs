﻿using HeroSyncServer.GameLogic.EventProcessing;
using System;
using System.Collections.Generic;
using System.Text;
using DarkRift;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncCommon.NetworkActions;
using HeroSyncServer.GameLogic.EventProcessing.Events;

namespace HeroSyncServer.GameLogic.GameActions
{
    public class EndTurnAction : GameAction
    {
        public override void TakeAction(Event e)
        {
            if (typeof(EndTurnEvent) != e.GetType())
            {
                gameRoom.dbConnect.logger.Error("Tried to end turn with a non-end-turn action.");
                return;
            }

            EndTurnEvent ete = (EndTurnEvent) e;
            PlayerData oldCurrentPlayer = gameRoom.currentPlayer;
            PlayerData newCurrentPlayer = gameRoom.EndTurn();

            string playerTurnMessage = String.Format("{0} Player's Turn", newCurrentPlayer.playerColor);
            EmptyMessage em = new EmptyMessage();
            em.optionalMessage = playerTurnMessage;
            gameRoom.SendMessage(em, NetworkStateTags.ServerMessageTags.GAME_START, SendMode.Reliable);


            if (ete.newPlayerShouldDraw)
            {
                DrawCardEvent dce = new DrawCardEvent(oldCurrentPlayer, newCurrentPlayer);
                gameRoom.AddEventToStack(dce);
            }
        }

        public EndTurnAction(ActionData baseData, GameRoom gr) : base(baseData, gr) { }
    }
}
