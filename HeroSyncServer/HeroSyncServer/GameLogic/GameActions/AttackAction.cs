﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using DarkRift;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.GameLogic.EventProcessing;
using HeroSyncServer.GameLogic.EventProcessing.Events;

namespace HeroSyncServer.GameLogic.GameActions
{
    public class AttackAction : GameAction
    {

        public AttackAction(ActionData ad, GameRoom gr) : base(ad, gr) { }

        public override void TakeAction(Event e)
        {
            if (e.GetType() != typeof(AttackEvent))
            {
                Console.Error.WriteLine("Tried to pass the Attack Action an incorrect event type.");
                return;
            }

            AttackEvent ae = (AttackEvent) e;
            CardData atkCard = ae.attackingCard;

            CardServerData csd = gameRoom.GetCardServerData(atkCard);
            csd.properties.Add(Property.HAS_ATTACKED);

            if (ae.defender == null)
            {
                Console.Error.WriteLine("Defender is null, abort abort");
            }


            if (ae.defender.GetType() == typeof(CardData))
            {
                CardData defCard = (CardData)ae.defender;
                CardBattle(atkCard, defCard, ae);
            }
            else if (ae.defender.GetType() == typeof(CharacterData))
            {
                CharacterData defender = (CharacterData) ae.defender;
                DirectAttack(atkCard, defender, ae);
            }
        }

        private void CardBattle(CardData atkCard, CardData defCard, AttackEvent ae)
        {
            CardData destroyedCard = null;

            if (atkCard.strength > defCard.strength)
            {
                destroyedCard = defCard;
            }
            else if (atkCard.strength < defCard.strength)
            {
                destroyedCard = atkCard;
            }
            else
            {
                FollowerKilledEvent fke1 = new FollowerKilledEvent(ae.GetSource(), atkCard);
                FollowerKilledEvent fke2 = new FollowerKilledEvent(ae.GetSource(), defCard);
                gameRoom.AddEventToStack(fke1);
                gameRoom.AddEventToStack(fke2);
                return;
            }

            if (destroyedCard != atkCard)
            {
                CardPermissionCalculator.SetCardPermissions(atkCard, gameRoom.cardStatusDictionary[atkCard]);
                gameRoom.SendMessage(atkCard, NetworkStateTags.ServerMessageTags.UPDATE_CARD_DATA, SendMode.Reliable);
            }

            FollowerKilledEvent fke = new FollowerKilledEvent(ae.GetSource(), destroyedCard);
            gameRoom.AddEventToStack(fke);
        }

        private void DirectAttack(CardData atkCard, CharacterData charData, AttackEvent ae)
        {
            CardPermissionCalculator.SetCardPermissions(atkCard, gameRoom.cardStatusDictionary[atkCard]);
            gameRoom.SendMessage(atkCard, NetworkStateTags.ServerMessageTags.UPDATE_CARD_DATA, SendMode.Reliable);
            int damage = -atkCard.strength;
            PlayerData affectedPlayer = gameRoom.orangePlayer.playerHero.gameId == charData.gameId ? gameRoom.orangePlayer:gameRoom.bluePlayer;
            InfluenceChangeEvent ice = new InfluenceChangeEvent(this, affectedPlayer, (short) damage);
            gameRoom.AddEventToStack(ice);
        }
    }
}
