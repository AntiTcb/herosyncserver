﻿using HeroSyncServer.GameLogic.EventProcessing;
using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon.CoreDataTypes;

namespace HeroSyncServer.GameLogic.GameActions.ActionMods
{
    public abstract class GameActionMod : GameAction
    {
        protected GameAction BaseGameAction;

        public GameActionMod(ActionData baseData, GameRoom game, GameAction baseGameAction) : base(baseData, game)
        {
            this.BaseGameAction = baseGameAction;
        }
    }
}
