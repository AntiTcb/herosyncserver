﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.GameLogic.EventProcessing;

namespace HeroSyncServer.GameLogic.GameActions
{
    public abstract class GameAction
    {
        public ActionData data;
        protected GameRoom gameRoom;

        public abstract void TakeAction(Event e);

        public GameAction(ActionData baseData, GameRoom game)
        {
            if (baseData == null)
            {
                throw new NoNullAllowedException("GameAction requires non-null gameAction data.");
            }
            if (game == null)
            {
                throw new NoNullAllowedException("GameAction requires non-null game room to act on.");
            }

            data = baseData;
            gameRoom = game;
        }

    }
}
