﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace HeroSyncServer.GameLogic.GameActions.Factories
{
    public class ChangeInfluenceActionFactory : ActionFactoryInterface
    {
        private ActionData.ActionTypeEnum actionType = ActionData.ActionTypeEnum.InfChange;

        public GameAction GenerateAction(GameRoom gr)
        {
            ActionData changeInfData = new ActionData();
            changeInfData.desc = "CHANGING INFLUENCE OF PLAYER";
            changeInfData.id = Guid.NewGuid();
            changeInfData.type = actionType;
            ChangeInfluenceAction cia = new ChangeInfluenceAction(changeInfData, gr);
            return cia;
        }

        public ActionData.ActionTypeEnum GetActionType()
        {
            return actionType;
        }
    }
}
