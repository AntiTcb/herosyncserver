﻿using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;

namespace HeroSyncServer.GameLogic.GameActions.Factories
{
    public interface ActionFactoryInterface
    {
        GameAction GenerateAction(GameRoom gr);
        ActionData.ActionTypeEnum GetActionType();
    }
}
