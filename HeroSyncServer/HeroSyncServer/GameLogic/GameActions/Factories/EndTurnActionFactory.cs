﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace HeroSyncServer.GameLogic.GameActions.Factories
{
    public class EndTurnActionFactory : ActionFactoryInterface
    {
        public GameAction GenerateAction(GameRoom gr)
        {
            ActionData a = new ActionData();
            a.type = GetActionType();
            a.desc = "Played Ending turn";
            a.id = Guid.NewGuid();

            EndTurnAction eta = new EndTurnAction(a, gr);
            return eta;
        }

        public ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.EndTurn;
        }
    }
}
