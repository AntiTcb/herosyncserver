﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HeroSyncServer.GameLogic.GameActions.Factories
{
    public class LowerFollowerPowerActionFactory : ActionFactoryInterface
    {
        private string weakenFollower = "Weaken Follower";

        public GameAction GenerateAction(GameRoom gr)
        {
            if (gr == null)
            {
                throw new NoNullAllowedException("Lower Follower Power Action Factory was passed a null game room; it needs to know what game to effect.");
            }

            ActionData actionData = new ActionData();
            actionData.id = Guid.NewGuid();
            actionData.type = GetActionType();
            actionData.desc = weakenFollower;
            LowerFollowerPowerGameAction lfp = new LowerFollowerPowerGameAction(actionData, gr);
            return lfp;
        }

        public ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.WeakenFollower;
        }
    }
}
