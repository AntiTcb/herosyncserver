﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace HeroSyncServer.GameLogic.GameActions.Factories
{
    public class DrawActionFactory : ActionFactoryInterface
    {
        public GameAction GenerateAction(GameRoom gr)
        {
            ActionData ad = new ActionData();
            ad.type = GetActionType();
            ad.desc = "Draw Card";
            ad.id = Guid.NewGuid();
            DrawCardAction dca = new DrawCardAction(ad, gr);
            return dca;
        }

        public ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.DrawCard;
        }
    }
}
