﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace HeroSyncServer.GameLogic.GameActions.Factories
{
    public class AttackActionFactory : ActionFactoryInterface
    {
        public static ActionData.ActionTypeEnum actionType = ActionData.ActionTypeEnum.Attack;

        public GameAction GenerateAction(GameRoom gr)
        {
            ActionData ad = new ActionData();
            ad.desc = "Fight between two cards";
            ad.id = Guid.NewGuid();
            ad.type = actionType;
            AttackAction attack = new AttackAction(ad, gr);
            return attack;
        }

        public ActionData.ActionTypeEnum GetActionType()
        {
            return actionType;
        }
    }
}
