﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace HeroSyncServer.GameLogic.GameActions.Factories
{
    public class KillFollowerActionFactory : ActionFactoryInterface
    {
        public string desc = "Kill Follower";

        public GameAction GenerateAction(GameRoom gr)
        {
            ActionData a = new ActionData();

            ActionData actionData = new ActionData();
            actionData.id = Guid.NewGuid();
            actionData.type = GetActionType();
            actionData.desc = desc;
            KillFollowerAction kfa = new KillFollowerAction(actionData, gr);
            return kfa;
        }

        public ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.KillFollower;
        }
    }
}
