﻿using HeroSyncServer.GameLogic.EventProcessing;
using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.GameLogic.EventProcessing.Events;

namespace HeroSyncServer.GameLogic.GameActions.Factories
{
    public class LowerFollowerPowerGameAction : GameAction
    {
        public override void TakeAction(Event e)
        {
            if (e.GetType() != typeof(CardPowerReductionEvent))
            {
                return;
            }

            CardPowerReductionEvent cpre = (CardPowerReductionEvent) e;
            CardData card = (CardData) e.GetTarget();
            card.strength -= (short) cpre.powerReduction;

            //TODO: Visually update card
        }

        public LowerFollowerPowerGameAction(ActionData a, GameRoom g) : base(a, g) { }
    }
}
