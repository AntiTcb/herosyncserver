﻿using HeroSyncServer.GameLogic.EventProcessing;
using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Text;
using DarkRift;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncCommon.NetworkActions;
using HeroSyncServer.GameLogic.EventProcessing.Events;

namespace HeroSyncServer.GameLogic.GameActions
{
    public class DrawCardAction : GameAction
    {
        public override void TakeAction(Event e)
        {
            DrawCardEvent dce = (DrawCardEvent) e;
            PlayerData drawingPlayer = dce.drawer;

            if (drawingPlayer.playerDeck.cards.Count <= 0)
            {
                gameRoom.PlayerLoss(drawingPlayer);
                return;
            }

            if (dce.cardToDraw == null)
            {
                dce.cardToDraw = drawingPlayer.playerDeck.cards[0];
            }

            if (!drawingPlayer.playerDeck.cards.Contains(dce.cardToDraw))
            {
                string errorMsg =
                    String.Format("Trying to draw card {0} from {1} player that is missing it from their deck.", dce.cardToDraw.englishName, drawingPlayer.playerColor);
                gameRoom.dbConnect.logger.Error(errorMsg);
                return;
            }

            CardData cardToDraw = dce.cardToDraw;
            drawingPlayer.playerDeck.cards.Remove(cardToDraw);
            drawingPlayer.cardsInHand.Add(cardToDraw);
            CardServerData csd = gameRoom.GetCardServerData(cardToDraw);
            csd.location = CardLocation.Hand;
            CardPermissionCalculator.SetCardPermissions(cardToDraw, csd);

            MoveCard moveCardFrontEndInfo = new MoveCard();
            moveCardFrontEndInfo.cardToMove = cardToDraw;
            moveCardFrontEndInfo.location = (drawingPlayer.playerColor == PlayerData.PlayerColor.BLUE)
                ? PlayerField.Zone.BlueHand
                : PlayerField.Zone.OrangeHand;
            moveCardFrontEndInfo.showCardBeforeMoving = false;
            gameRoom.SendMessage(moveCardFrontEndInfo, NetworkStateTags.ServerMessageTags.MOVE_CARD_GENERIC, SendMode.Reliable);
        }

        public DrawCardAction(ActionData baseData, GameRoom gameRoom) : base(baseData, gameRoom) { }
    }
}
