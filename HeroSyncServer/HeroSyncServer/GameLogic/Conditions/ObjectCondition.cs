﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Google.Protobuf.Reflection;
using Google.Protobuf.WellKnownTypes;
using HeroSyncServer.DatabaseHandling;
using Type = System.Type;

namespace HeroSyncServer.GameLogic.EventProcessing.Processors.EventEvaluators
{
    public class ObjectCondition
    {
        public FieldInfo field;
        public string fieldName;
        public bool isUsingID;

        public string requiredValueRegex;
        public int requiredValueMinimum;
        public int requiredValueMaximum;
        public int idValue;

        public bool MeetsCondition(Object obj)
        {
            field = obj.GetType().GetField(fieldName);
            if (field == null)
            {
                return false;
            }
            if (field.FieldType == typeof(string))
            {
                return Regex.IsMatch((string)field.GetValue(obj) , requiredValueRegex);
            }

            if (field.FieldType == typeof(int))
            {
                int value = (int) field.GetValue(obj);
                if (isUsingID)
                {
                    return value == idValue;
                }
                else
                {
                    return requiredValueMinimum >= value && value >= requiredValueMaximum;
                }
            }
            Console.Error.Write(String.Format("Condition has a field of type {0} which cannot be analyzed by ObjectCondition. Failing condition. ", field.Name));
            return false;
        }

        public void SetValuesFromDatabase(DBConditionCategoryRepresentation dbRepresentation)
        {
            fieldName = dbRepresentation.field;
            requiredValueRegex = dbRepresentation.stringValue;
            isUsingID = dbRepresentation.usingId;
            if (isUsingID)
            {
                idValue = dbRepresentation.category_unique_id;
            }
            else
            {
                requiredValueMinimum = dbRepresentation.minValue;
                requiredValueMaximum = dbRepresentation.maxValue;
            }
        }
    }
}
