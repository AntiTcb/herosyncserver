﻿using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;
using HeroSyncServer.GameLogic.EventProcessing.Processors;

namespace HeroSyncServer.GameLogic
{
    public class CardServerData
    {
        public CardData baseData;
        public CardLocation location;
        public PlayerData owner;
        public List<CardData> debuffSources;
        public CardData[] buffSources;
        public bool activationLegallity;
        public List<Property> properties;
        public List<EventProcessor> cardEffects;
        public PlayerField.Zone currentZone;

        public CardServerData(CardData card, PlayerData player)
        {
            baseData = card;
            buffSources = new CardData[3];
            debuffSources = new List<CardData>();
            owner = player;
            properties = new List<Property>();
            cardEffects = new List<EventProcessor>();
            properties = new List<Property>();
        }

        public void UpdateLevelLegality()
        {
            short playerLevel = owner.playerHero.level;
            short cardLevel = baseData.level;

            activationLegallity = playerLevel >= cardLevel;
        }

    }
}
