using System;
using System.Collections.Generic;
using System.Reflection;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncCommon.GameLogic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HeroSyncServer;
using HeroSyncServer.DatabaseHandling;
using HeroSyncServer.GameLogic;
using HeroSyncServer.GameLogic.EventProcessing;
using HeroSyncServer.GameLogic.EventProcessing.Events;
using HeroSyncServer.GameLogic.EventProcessing.Processors;
using HeroSyncServer.GameLogic.EventProcessing.Processors.EventEvaluators;
using HeroSyncServer.GameLogic.GameActions;
using HeroSyncServer.GameLogic.Targeting;

namespace ServerLogicTest.EventTests
{

    /**
     * This test class is for determining if events and scenarios are processed properly.
     */

    [TestClass]
    public class EventProcessingTests
    {

        /**
         * This tests the basic idea of adding a SummonIntentEvent to the stack and seeing a follower summoned.
         */

        [TestMethod]
        public void SummonEventTest()
        {
            GameRoom gr = SetupDefaultGameRoom();
        
            CardData tempCard = new CardData();
            tempCard.gameID = 1;
            PlayerField.Zone targetZone = PlayerField.Zone.BlueFollowerSlot1;
            CardServerData tempServerData = new CardServerData(tempCard, gr.bluePlayer);
            gr.cardStatusDictionary.Add(tempCard, tempServerData);

            SummonEvent e = new SummonEvent(this, tempCard, targetZone);
            gr.AddEventToStack(e);

            CardData foundCard = gr.gameField.cardsInPlay[targetZone];
            Assert.AreEqual(tempCard, foundCard);
        }
        
        [TestMethod]
        public void PlayDriveToDestroyFollower()
        {
            GameRoom gr = SetupDefaultGameRoom();
            CardData drive = new CardData();
            drive.gameID = 1;
            drive.type = CardType.Drive;
            drive.englishName = "Test Drive";
            drive.availableActions = new List<CardActionEnum>();
            drive.availableActions.Add(CardActionEnum.Play);

            CardData defaultFollower = new CardData();
            defaultFollower.gameID = 2;
            defaultFollower.type = CardType.Follower;
            gr.cardStatusDictionary.Add(defaultFollower, new CardServerData(defaultFollower, gr.orangePlayer));
            gr.AddCardToZone(defaultFollower, PlayerField.Zone.OrangeFollowerSlot1);

            EventProcessor testDriveEventProcessor = new EventProcessor(gr);
            testDriveEventProcessor.timing = Timing.NotApplicable;
            testDriveEventProcessor.triggers= new List<EventTriggerEvaluator>();
            testDriveEventProcessor.triggers.Add(new SelfPlayedTriggerEvaluator());

            testDriveEventProcessor.actionEvents = new List<(Event, Targeter)>();
            Event killFollowerEvent = new FollowerKilledEvent(drive, defaultFollower);
            Targeter targeter = new NoTargeter();
            testDriveEventProcessor.actionEvents.Add((killFollowerEvent, targeter));

            CardServerData driveServerData = new CardServerData(drive, gr.bluePlayer);
            driveServerData.cardEffects.Add(testDriveEventProcessor);
            gr.cardStatusDictionary.Add(drive, driveServerData);
            
        
            PlayDriveEvent die = new PlayDriveEvent(this, drive);
            gr.AddEventToStack(die);

            PlayerField.Zone cardZone = gr.cardStatusDictionary[defaultFollower].currentZone;
            Assert.AreEqual(PlayerField.Zone.OrangeFallenArea, cardZone);
        }


        [TestMethod]
        public void AttackOpponentMonsterTest()
        {
            GameRoom gr = SetupDefaultGameRoom();

            CardData opponentCard = new CardData();
            opponentCard.type = CardType.Follower;
            opponentCard.strength = 20;
            opponentCard.level = 1;
            opponentCard.gameID = 123;
            opponentCard.englishName = "Test Opponent Monster A";
            CardServerData oppCardServerData = new CardServerData(opponentCard, gr.orangePlayer);
            gr.cardStatusDictionary.Add(opponentCard, oppCardServerData);
            gr.AddCardToZone(opponentCard, PlayerField.Zone.OrangeFollowerSlot1);

            CardData playerCard = new CardData();
            playerCard.type = CardType.Follower;
            playerCard.strength = 30;
            playerCard.level = 1;
            playerCard.gameID = 124;
            playerCard.englishName = "Test Player Monster A";
            CardServerData playerCardServerData = new CardServerData(playerCard, gr.bluePlayer);
            gr.cardStatusDictionary.Add(playerCard, playerCardServerData);
            gr.AddCardToZone(playerCard, PlayerField.Zone.BlueFollowerSlot1);

            AttackTargetEvent ate = new AttackTargetEvent(gr.bluePlayer, playerCard, opponentCard);
            gr.AddEventToStack(ate);
            gr.AddEventToStack(ate.resultingEvent);
            PlayerField.Zone defeatedMonsterZone = gr.cardStatusDictionary[opponentCard].currentZone;
            Assert.AreEqual(PlayerField.Zone.OrangeFallenArea, defeatedMonsterZone);

            short orangeInf = gr.orangePlayer.influence;
            Assert.AreEqual(390, orangeInf);
        }

        [TestMethod]
        public void EndTurnTest()
        {
            GameRoom gr = SetupDefaultGameRoom();
            gr.testMode = true;

            gr.leadingPlayer = gr.bluePlayer;
            gr.SetCurrentPlayer(gr.leadingPlayer);
            short oldLevel = gr.bluePlayer.playerHero.level;
            Random r = new Random();
            AddBlankCardToDeck(gr.bluePlayer, r, gr);
            AddBlankCardToDeck(gr.orangePlayer, r, gr);

            CardData orangeTopCard = gr.orangePlayer.playerDeck.cards[0];

            EndTurnEvent ete = new EndTurnEvent(gr.bluePlayer);
            gr.AddEventToStack(ete);
            PlayerData currPlayer = gr.currentPlayer;
            short currLevel = gr.bluePlayer.playerHero.level;

            Assert.AreEqual(gr.orangePlayer, currPlayer);
            Assert.AreEqual(oldLevel, currLevel);
            Assert.AreEqual(0, gr.orangePlayer.playerDeck.cards.Count);
            Assert.IsTrue(gr.orangePlayer.cardsInHand.Contains(orangeTopCard));

            EndTurnEvent secondEvent = new EndTurnEvent(gr.orangePlayer);
            gr.AddEventToStack(secondEvent);

            short newLevel = gr.bluePlayer.playerHero.level;
            Assert.AreNotEqual(oldLevel, newLevel);
        }

        internal static GameRoom SetupDefaultGameRoom()
        {
            GameRoom gr = new GameRoom();
            gr.testMode = true;
        
            PlayerData bluePlayer = new PlayerData();
            bluePlayer.playerDeck = new Deck();
            bluePlayer.influence = 400;
            bluePlayer.playerHero = new CharacterData();
            bluePlayer.playerHero.level = 1;
            bluePlayer.cardsInHand = new List<CardData>();

            PlayerData orangePlayer = new PlayerData();
            orangePlayer.influence = 400;
            orangePlayer.playerDeck = new Deck();
            orangePlayer.playerHero = new CharacterData();
            orangePlayer.playerHero.level = 1;
            orangePlayer.cardsInHand = new List<CardData>();

            gr.bluePlayer = bluePlayer;
            gr.orangePlayer = orangePlayer;
        
            gr.SetupGameRoomGlobalReqs();
            return gr;
        }

        internal void AddBlankCardToDeck(PlayerData p, Random r, GameRoom gr)
        {
            CardData extraCard = new CardData();
            short id = (short) r.Next();
            extraCard.gameID = id;
            p.playerDeck.cards.Add(extraCard);
            gr.cardStatusDictionary.Add(extraCard, new CardServerData(extraCard, p));
        }
    }
}
