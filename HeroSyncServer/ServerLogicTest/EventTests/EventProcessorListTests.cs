﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Google.Protobuf.WellKnownTypes;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer;
using HeroSyncServer.DatabaseHandling;
using HeroSyncServer.GameLogic;
using HeroSyncServer.GameLogic.EventProcessing;
using HeroSyncServer.GameLogic.EventProcessing.Events;
using HeroSyncServer.GameLogic.EventProcessing.Processors;
using HeroSyncServer.GameLogic.EventProcessing.Processors.EventEvaluators;
using HeroSyncServer.GameLogic.GameActions;
using HeroSyncServer.GameLogic.GameActions.ActionMods;
using HeroSyncServer.GameLogic.GameActions.Factories;
using HeroSyncServer.GameLogic.Targeting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Org.BouncyCastle.Asn1.X509;
using Type = System.Type;

namespace ServerLogicTest.EventTests
{

    [TestClass]
    public class EventProcessorListTests
    {

        [TestMethod]
        public void TestEventProcessorListOrdering()
        {
            GameRoom gr = EventProcessingTests.SetupDefaultGameRoom();
            EventProcessorList eventProcessorList = new EventProcessorList(gr);
            CardData follower = new CardData();
            follower.ID = 1;
            follower.type = CardType.Follower;
            follower.gameID = 1;
            follower.strength = 30;
            
            CardServerData csd = new CardServerData(follower, gr.bluePlayer);
            gr.cardStatusDictionary.Add(follower, csd);

            EventTriggerEvaluator baseTrigger = new DefaultSuccessTriggerEvaluator();
            
            Event lowerPowerEvent = new CardPowerReductionEvent(this, follower, 10);

            Targeter lowestFollowerTargeter = new TargetWeakestFollower();

            EventProcessor whenSummonedProcessor = new EventProcessor(gr);
            whenSummonedProcessor.timing = Timing.When;
            whenSummonedProcessor.ID = Guid.NewGuid();
            EventTriggerEvaluator whenSummonEvaluator = new SummonTriggerEvaluator(new DBConditionRepresentation(), baseTrigger);
            whenSummonedProcessor.triggers = new List<EventTriggerEvaluator>() { whenSummonEvaluator };
            whenSummonedProcessor.actionEvents = new List<(Event, Targeter)>() { (lowerPowerEvent, lowestFollowerTargeter) };
        
            EventProcessor afterSummonedProcessor = new EventProcessor(gr);
            afterSummonedProcessor.timing = Timing.After;
            afterSummonedProcessor.ID = Guid.NewGuid();
            EventTriggerEvaluator afterSummonEvaluator = new SummonTriggerEvaluator(new DBConditionRepresentation(), baseTrigger);
            afterSummonedProcessor.triggers = new List<EventTriggerEvaluator>() {afterSummonEvaluator};
            afterSummonedProcessor.actionEvents = new List<(Event, Targeter)>() { (lowerPowerEvent, lowestFollowerTargeter) };

            gr.AddEventProcessor(whenSummonedProcessor);
            gr.AddEventProcessor(afterSummonedProcessor);

            SummonEvent e = new SummonEvent(this, follower, PlayerField.Zone.BlueFollowerSlot1);
        
            gr.AddEventToStack(e);
            Assert.AreEqual(10, follower.strength);
        }
    }
}
