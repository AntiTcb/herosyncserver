CREATE TABLE `effects` (
  `id` int(11) NOT NULL,
  `description` varchar(255),
  `timing` ENUM('Not Set', 'When', 'After', 'During', 'Not Applicable'),
  primary key(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `location` varchar(255),
  primary key(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `effect_applicable_locations` (
  `id` int(11) NOT NULL,
  `location_id` int(11),
  `effect_id` int(11),
  primary key (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `card_effect_pairs` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
    `card_id` int(11) NOT NULL,
    `effect_id` int(11) NOT NULL,
    primary key(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `effect_trigger_pairs` (
	`id` int(11) NOT NULL auto_increment,
    `effect_id` int(11) NOT NULL,
    `trigger_id` int(11) NOT NULL,
    primary key(`id`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `triggers` (
	`id` int(11) NOT NULL auto_increment,
    `hasSourceCondition` bool,
    `hasTargetCondition` bool,
    primary key(`id`)
)ENGINE=MyISAM DEFAULT CHARSET=utf8; 

CREATE TABLE `conditions` (
	`id` int(11) NOT NULL auto_increment,
	`description` varchar(255),
    `effect_type` varchar(255),
    `effect_id` int(11),
    `isChoice` bool,
    primary key(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `condition_categories` (
	`id` int (11) NOT NULL auto_increment,
    `condition_id` int(11) NOT NULL,
    `category_id` int(11) NOT NULL,
    primary key(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `category_types` (
	`id` int (11) NOT NULL auto_increment,
    `field` varchar(255),
    `string_value` varchar(255),
    `min_value` int(11),
    `max_value` int(11),
    `id_specific_value` int(11),
    `uses_id` bool,
    primary key(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `action enums` (
	`id` int (11) NOT NULL auto_increment,
    `effect` varchar(255),
    primary key(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

