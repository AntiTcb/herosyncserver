INSERT INTO `herosyncdemo`.`cards`
(
`name`,
`level`,
`effect`,
`power`,
`hero`,
`card_type`,
`pic`,
`hero_id`)
VALUES
(
"Spike Pit",
2,
"When an opponent attacks a follower adjacent to this set counter, move the attacked follower to the other adjacent zone. If you do, destroy the attacking monster.",
0,
"TemporMary",
"Counter",
"temp",
1);
