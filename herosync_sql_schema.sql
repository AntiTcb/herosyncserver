-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 12, 2020 at 06:45 PM
-- Server version: 10.3.22-MariaDB-1ubuntu1
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `herosync-demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `abilities`
--

CREATE TABLE `abilities` (
  `id` int(11) NOT NULL,
  `hero` int(11) NOT NULL DEFAULT 0,
  `talent` varchar(100) NOT NULL DEFAULT '',
  `effect` varchar(1000) NOT NULL DEFAULT '',
  `type` varchar(100) NOT NULL DEFAULT '',
  `level` int(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE `cards` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `level` int(11) NOT NULL,
  `effect` varchar(1000) NOT NULL DEFAULT '',
  `power` int(11) NOT NULL,
  `hero` varchar(100) NOT NULL DEFAULT '',
  'hero_id' int(11) NOT NULL,
  `card_type` varchar(100) NOT NULL DEFAULT '',
  `pic` varchar(100) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `decks`
--

CREATE TABLE `decks` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `username` varchar(35) DEFAULT NULL,
  `deck_name` varchar(35) DEFAULT NULL,
  `main_1` int(11) DEFAULT 0,
  `main_2` int(11) DEFAULT 0,
  `main_3` int(11) DEFAULT 0,
  `main_4` int(11) DEFAULT 0,
  `main_5` int(11) DEFAULT 0,
  `main_6` int(11) DEFAULT 0,
  `main_7` int(11) DEFAULT 0,
  `main_8` int(11) DEFAULT 0,
  `main_9` int(11) DEFAULT 0,
  `main_10` int(11) DEFAULT 0,
  `main_11` int(11) DEFAULT 0,
  `main_12` int(11) DEFAULT 0,
  `main_13` int(11) DEFAULT 0,
  `main_14` int(11) DEFAULT 0,
  `main_15` int(11) DEFAULT 0,
  `main_16` int(11) DEFAULT 0,
  `main_17` int(11) DEFAULT 0,
  `main_18` int(11) DEFAULT 0,
  `main_19` int(11) DEFAULT 0,
  `main_20` int(11) DEFAULT 0,
  `main_21` int(11) DEFAULT 0,
  `main_22` int(11) DEFAULT 0,
  `main_23` int(11) DEFAULT 0,
  `main_24` int(11) DEFAULT 0,
  `main_25` int(11) DEFAULT 0,
  `main_26` int(11) DEFAULT 0,
  `main_27` int(11) DEFAULT 0,
  `main_28` int(11) DEFAULT 0,
  `main_29` int(11) DEFAULT 0,
  `main_30` int(11) DEFAULT 0,
  `main_31` int(11) DEFAULT 0,
  `main_32` int(11) DEFAULT 0,
  `main_33` int(11) DEFAULT 0,
  `main_34` int(11) DEFAULT 0,
  `main_35` int(11) DEFAULT 0,
  `main_36` int(11) DEFAULT 0,
  `main_37` int(11) DEFAULT 0,
  `main_38` int(11) DEFAULT 0,
  `main_39` int(11) DEFAULT 0,
  `main_40` int(11) DEFAULT 0,
  `main_41` int(11) DEFAULT 0,
  `main_42` int(11) DEFAULT 0,
  `main_43` int(11) DEFAULT 0,
  `main_44` int(11) DEFAULT 0,
  `main_45` int(11) DEFAULT 0,
  `starter_deck` int(11) DEFAULT 0,
  `hero` int(11) NOT NULL DEFAULT 1,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `duels`
--

CREATE TABLE `duels` (
  `id` int(11) NOT NULL,
  `duel_id` int(11) NOT NULL DEFAULT 0,
  `type` varchar(20) DEFAULT 's',
  `format` varchar(20) DEFAULT '',
  `winner_id` int(11) DEFAULT NULL,
  `draw` tinyint(1) NOT NULL DEFAULT 0,
  `p1_id` int(11) NOT NULL DEFAULT 0,
  `p2_id` int(11) NOT NULL DEFAULT 0,
  `p1_deck` int(11) NOT NULL DEFAULT 0,
  `p2_deck` int(11) NOT NULL DEFAULT 0,
  `start_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_private` bit(1) NOT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `heroes`
--

CREATE TABLE `heroes` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `artwork` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE `logins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `username` varchar(35) NOT NULL DEFAULT '',
  `session` varchar(40) DEFAULT NULL,
  `password` varchar(40) NOT NULL DEFAULT '',
  `ip_address` varchar(100) NOT NULL DEFAULT '',
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `level` int(11) NOT NULL,
  `effect` varchar(1000) NOT NULL DEFAULT '',
  `power` int(11) NOT NULL,
  `hero` varchar(100) NOT NULL DEFAULT '',
  `card_type` varchar(100) NOT NULL DEFAULT '',
  `pic` varchar(100) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(35) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT 0,
  `confirmation_id` varchar(32) NOT NULL DEFAULT '',
  `reset_password_id` varchar(40) NOT NULL DEFAULT '',
  `join_date` datetime DEFAULT NULL,
  `ip_address` varchar(100) DEFAULT NULL,
  `last_login_date` timestamp NULL DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  `admin` int(11) NOT NULL DEFAULT 0,
  `default_deck` varchar(35) NOT NULL DEFAULT 'Untitled'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abilities`
--
ALTER TABLE `abilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `decks`
--
ALTER TABLE `decks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `duels`
--
ALTER TABLE `duels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p1_id` (`p1_id`),
  ADD KEY `p2_id` (`p2_id`),
  ADD KEY `winner_id` (`winner_id`),
  ADD KEY `end_time` (`end_time`),
  ADD KEY `duel_id` (`duel_id`);

--
-- Indexes for table `heroes`
--
ALTER TABLE `heroes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logins`
--
ALTER TABLE `logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abilities`
--
ALTER TABLE `abilities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `decks`
--
ALTER TABLE `decks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `duels`
--
ALTER TABLE `duels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `heroes`
--
ALTER TABLE `heroes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logins`
--
ALTER TABLE `logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
